﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BetaPlast.App_Code.Classes;
using BetaPlast;
using System.Data;

namespace BetaPlast.App_Code.Persistencia
{
    public class MateriaPrimaBD
    {


        public MateriaPrima Select(int id)
        {
            MateriaPrima materiaPrima = new MateriaPrima();
            try
            {

                System.Data.IDbCommand objCommand;
                System.Data.IDbConnection objConnection;
                System.Data.IDataReader objDataReader;

                objConnection = Mapped.Connection();
                objCommand = Mapped.Command("select * from tbl_materiaprima where map_codigo = ?id;", objConnection);
                objCommand.Parameters.Add(Mapped.Parameter("?id", id));

                objDataReader = objCommand.ExecuteReader();
                while (objDataReader.Read())
                {
                    FornecedorBD bd = new FornecedorBD();
                    materiaPrima.Codigo = Convert.ToInt32(objDataReader["map_codigo"]);
                    materiaPrima.Nome = Convert.ToString(objDataReader["map_nome"]);
                    materiaPrima.Cor = Convert.ToString(objDataReader["map_cor"]);
                    materiaPrima.QuantidadeAtual = Convert.ToDouble(objDataReader["map_quantidadeatual"]);
                    materiaPrima.QuantidadeMinima = Convert.ToDouble(objDataReader["map_quantidademinima"]);
                    materiaPrima.Peso = Convert.ToDouble(objDataReader["map_pesoporkg"]);
                    materiaPrima.Fornecedor = bd.SelectByID(Convert.ToInt32(objDataReader["for_codigo"]));
                }
            }
            catch (Exception)
            {
                throw;
            }

            return materiaPrima;
        }

        public MateriaPrima SelectByNome(string nome)
        {
            MateriaPrima materiaPrima = new MateriaPrima();
            try
            {

                System.Data.IDbCommand objCommand;
                System.Data.IDbConnection objConnection;
                System.Data.IDataReader objDataReader;

                objConnection = Mapped.Connection();
                objCommand = Mapped.Command("select * from tbl_materiaprima where map_nome = ?nome;", objConnection);
                objCommand.Parameters.Add(Mapped.Parameter("?nome", nome));

                objDataReader = objCommand.ExecuteReader();
                while (objDataReader.Read())
                {
                    FornecedorBD bd = new FornecedorBD();
                    materiaPrima.Codigo = Convert.ToInt32(objDataReader["map_codigo"]);
                    materiaPrima.Nome = Convert.ToString(objDataReader["map_nome"]);
                    materiaPrima.Cor = Convert.ToString(objDataReader["map_cor"]);
                    materiaPrima.QuantidadeAtual = Convert.ToDouble(objDataReader["map_quantidadeatual"]);
                    materiaPrima.QuantidadeMinima = Convert.ToDouble(objDataReader["map_quantidademinima"]);
                    materiaPrima.Peso = Convert.ToDouble(objDataReader["map_pesoporkg"]);
                    materiaPrima.Fornecedor = bd.SelectByID(Convert.ToInt32(objDataReader["for_codigo"]));
                }
            }
            catch (Exception)
            {
                throw;
            }

            return materiaPrima;
        }

        public DataSet SelectAll()
        {
            DataSet ds = new DataSet();
            try
            {
                System.Data.IDbCommand objCommand;
                System.Data.IDbConnection objConnection;
                System.Data.IDataAdapter objDataAdapter;

                objConnection = Mapped.Connection();
                objCommand = Mapped.Command("select * from tbl_materiaprima inner join tbl_fornecedor  using(for_codigo);", objConnection);

                objDataAdapter = Mapped.Adapter(objCommand);
                objDataAdapter.Fill(ds);

                objConnection.Close();
                objConnection.Dispose();
                objCommand.Dispose();
            }
            catch (Exception)
            {

                throw;
            }

            return ds;
        }

        public bool Update(MateriaPrima materiaprima)
        {
            bool retorno = true;
            try
            {
                System.Data.IDbCommand objCommand;
                System.Data.IDbConnection objConnection;

                string update = "UPDATE tbl_materiaprima SET map_cor=?cor, for_codigo=?fornecedor, map_nome=?nome, map_quantidadeatual=?quantidadeatual, map_quantidademinima=?quantidademinima, map_pesoporkg=?peso WHERE map_codigo=?codigo;";

                objConnection = Mapped.Connection();

                objCommand = Mapped.Command(update, objConnection);

                objCommand.Parameters.Add(Mapped.Parameter("?cor", materiaprima.Cor));
                objCommand.Parameters.Add(Mapped.Parameter("?fornecedor", materiaprima.Fornecedor.Codigo));
                objCommand.Parameters.Add(Mapped.Parameter("?nome", materiaprima.Nome));
                objCommand.Parameters.Add(Mapped.Parameter("?quantidadeatual", materiaprima.QuantidadeAtual));
                objCommand.Parameters.Add(Mapped.Parameter("?quantidademinima", materiaprima.QuantidadeMinima));
                objCommand.Parameters.Add(Mapped.Parameter("?peso", materiaprima.Peso));
                objCommand.Parameters.Add(Mapped.Parameter("?codigo", materiaprima.Codigo));

                objCommand.ExecuteNonQuery();

                objConnection.Close();
                objConnection.Dispose();
                objCommand.Dispose();

            }
            catch (Exception)
            {
                retorno = false;
                throw;
            }

            return retorno;
        }

        public MateriaPrimaBD()
        {
            //
            // TODO: Adicionar lógica do construtor aqui
            //
        }
    }
}