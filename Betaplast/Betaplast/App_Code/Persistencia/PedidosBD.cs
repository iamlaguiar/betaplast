﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BetaPlast.App_Code.Classes;
using System.Data;
using BetaPlast;

namespace BetaPlast.App_Code.Persistencia
{

    public class PedidosBD
    {
        public bool InsertPedido(Pedido pedido)
        {
            bool retorno = true;
            try
            {
                System.Data.IDbCommand objCommand;
                System.Data.IDbConnection objConnection;

                string insert = "insert into tbl_pedido (ped_descricao, ped_quantidade, ped_datapedido, ped_dataentrega, ped_observacoes, cli_codigo, pro_codigo, ped_status) values (?descricao, ?quantidade,  now(),  ?dataentrega, " + "?observacoes, ?cliente, ?produto, ?status); ";
                objConnection = Mapped.Connection();
                objCommand = Mapped.Command(insert, objConnection);
                objCommand.Parameters.Add(Mapped.Parameter("?descricao", pedido.Descricao));
                objCommand.Parameters.Add(Mapped.Parameter("?quantidade", pedido.Quantidade));
                objCommand.Parameters.Add(Mapped.Parameter("?dataentrega", pedido.DataEntrega.ToString("yyyy-MM-dd")));
                objCommand.Parameters.Add(Mapped.Parameter("?observacoes", pedido.Obersevacoes));
                objCommand.Parameters.Add(Mapped.Parameter("?status", pedido.Status));
                objCommand.Parameters.Add(Mapped.Parameter("?cliente", pedido.Cliente.Codigo));
                objCommand.Parameters.Add(Mapped.Parameter("?produto", pedido.Produto.Codigo));

                objCommand.ExecuteNonQuery();

                objCommand.Dispose();
                objConnection.Close();
                objConnection.Dispose();
            }
            catch (Exception ex)
            {
                retorno = false;
                throw;
            }

            return retorno;
        }


        public DataSet SelectAll()
        {
            DataSet ds = new DataSet();

            try
            {
                System.Data.IDbCommand objCommand;
                System.Data.IDbConnection objConnection;
                System.Data.IDataAdapter objDataAdapter;

                objConnection = Mapped.Connection();
                objCommand = Mapped.Command("select * from tbl_pedido;", objConnection);

                objDataAdapter = Mapped.Adapter(objCommand);
                objDataAdapter.Fill(ds);

                objCommand.Dispose();
                objConnection.Close();
                objConnection.Dispose();


            }
            catch (Exception)
            {
                throw;
            }

            return ds;
        }


        public DataSet SelectNaoRecebidos()
        {
            DataSet ds = new DataSet();

            try
            {
                System.Data.IDbCommand objCommand;
                System.Data.IDbConnection objConnection;
                System.Data.IDataAdapter objDataAdapter;

                objConnection = Mapped.Connection();
                objCommand = Mapped.Command("select tbl_pedido.*, pro_descricao from tbl_pedido  inner join tbl_produto on tbl_pedido.pro_codigo = tbl_produto.pro_codigo where ped_status = 'Registrado';", objConnection);

                objDataAdapter = Mapped.Adapter(objCommand);
                objDataAdapter.Fill(ds);

                objCommand.Dispose();
                objConnection.Close();
                objConnection.Dispose();


            }
            catch (Exception)
            {
                throw;
            }

            return ds;
        }

        public DataSet SelectAllWithProduto()
        {
            DataSet ds = new DataSet();

            try
            {
                System.Data.IDbCommand objCommand;
                System.Data.IDbConnection objConnection;
                System.Data.IDataAdapter objDataAdapter;

                objConnection = Mapped.Connection();
                objCommand = Mapped.Command("select tbl_pedido.*, pro_descricao from tbl_pedido  inner join tbl_produto on tbl_pedido.pro_codigo = tbl_produto.pro_codigo where ped_status <> 'Registrado'", objConnection);

                objDataAdapter = Mapped.Adapter(objCommand);
                objDataAdapter.Fill(ds);

                objCommand.Dispose();
                objConnection.Close();
                objConnection.Dispose();


            }
            catch (Exception)
            {
                throw;
            }

            return ds;
        }

        public Pedido Select(int id)
        {
            Pedido pedido = new Pedido();
            try
            {
                System.Data.IDbCommand objCommand;
                System.Data.IDbConnection objConnection;
                System.Data.IDataReader objDataReader;

                objConnection = Mapped.Connection();
                objCommand = Mapped.Command("select * from tbl_pedido where ped_codigo = ?id;", objConnection);
                objCommand.Parameters.Add(Mapped.Parameter("?id", id));

                objDataReader = objCommand.ExecuteReader();
                while (objDataReader.Read())
                {
                    ProdutoBD produtobd = new ProdutoBD();
                    ClienteBD clientebd = new ClienteBD();
                    pedido.Codigo = Convert.ToInt32(objDataReader["ped_codigo"]);
                    pedido.Descricao = Convert.ToString(objDataReader["ped_descricao"]);
                    pedido.DataEntrega = Convert.ToDateTime(objDataReader["ped_dataentrega"]);
                    pedido.DataPedido = Convert.ToDateTime(objDataReader["ped_datapedido"]);
                    pedido.Obersevacoes = Convert.ToString(objDataReader["ped_observacoes"]);
                    pedido.Status = Convert.ToString(objDataReader["ped_status"]);
                    pedido.Cliente = clientebd.Select(Convert.ToInt32(objDataReader["cli_codigo"]));
                    pedido.Produto = produtobd.Select(Convert.ToInt32(objDataReader["pro_codigo"]));
                }

                objCommand.Dispose();
                objConnection.Close();
                objConnection.Dispose();
            }
            catch (Exception)
            {
                throw;
            }

            return pedido;
        }

        public bool FinalizarPedido(int codigo)
        {
            bool retorno = true;
            try
            {
                System.Data.IDbCommand objCommand;
                System.Data.IDbConnection objConnection;

                objConnection = Mapped.Connection();
                objCommand = Mapped.Command("update tbl_pedido set ped_status = 'Finalizado' where ped_codigo = ?codigo;", objConnection);
                objCommand.Parameters.Add(Mapped.Parameter("?codigo", codigo));

                objCommand.ExecuteNonQuery();

                objCommand.Dispose();
                objConnection.Close();
                objConnection.Dispose();
            }
            catch (Exception)
            {
                retorno = false;
                throw;
            }

            return retorno;
        }

        public bool ConfirmarRecebimento(int codigo)
        {
            bool retorno = true;
            try
            {
                System.Data.IDbCommand objCommand;
                System.Data.IDbConnection objConnection;

                objConnection = Mapped.Connection();
                objCommand = Mapped.Command("update tbl_pedido set ped_status = 'Recebido' where ped_codigo = ?codigo;", objConnection);
                objCommand.Parameters.Add(Mapped.Parameter("?codigo", codigo));

                objCommand.ExecuteNonQuery();

                objCommand.Dispose();
                objConnection.Close();
                objConnection.Dispose();
            }
            catch (Exception)
            {
                retorno = false;
                throw;
            }

            return retorno;
        }

        public bool CancelarPedido(int codigo)
        {
            bool retorno = true;
            try
            {
                System.Data.IDbCommand objCommand;
                System.Data.IDbConnection objConnection;

                objConnection = Mapped.Connection();
                objCommand = Mapped.Command("delete from tbl_pedido where ped_codigo = ?codigo;", objConnection);
                objCommand.Parameters.Add(Mapped.Parameter("?codigo", codigo));

                objCommand.ExecuteNonQuery();

                objCommand.Dispose();
                objConnection.Close();
                objConnection.Dispose();
            }
            catch (Exception)
            {
                retorno = false;
                throw;
            }

            return retorno;
        }

        public bool UpdatePedido(Pedido pedido)
        {
            bool retorno = true;
            try
            {
                System.Data.IDbCommand objCommand;
                System.Data.IDbConnection objConnection;

                string sql = "update tbl_pedido set ped_descricao = ?descricao, ped_quantidade = ?quantidade, ped_datapedido = ?datapedido, ped_dataentrega =?dataentrega, ped_observacoes=?observacoes, " + "cli_codigo =?cliente, pro_codigo =?produto, ped_status = ?status where ped_codigo =?codigo";

                objConnection = Mapped.Connection();
                objCommand = Mapped.Command(sql, objConnection);

                objCommand.Parameters.Add(Mapped.Parameter("?descricao", pedido.Descricao));
                objCommand.Parameters.Add(Mapped.Parameter("?quantidade", pedido.Quantidade));
                objCommand.Parameters.Add(Mapped.Parameter("?datapedido", pedido.DataPedido));
                objCommand.Parameters.Add(Mapped.Parameter("?dataentrega", pedido.DataEntrega));
                objCommand.Parameters.Add(Mapped.Parameter("?observacoes", pedido.Obersevacoes));
                objCommand.Parameters.Add(Mapped.Parameter("?cliente", pedido.Cliente.Codigo));
                objCommand.Parameters.Add(Mapped.Parameter("?produto", pedido.Produto.Codigo));
                objCommand.Parameters.Add(Mapped.Parameter("?status", pedido.Status));
                objCommand.Parameters.Add(Mapped.Parameter("?codigo", pedido.Codigo));

                objCommand.ExecuteNonQuery();
                objCommand.Dispose();
                objConnection.Close();
                objConnection.Dispose();
            }
            catch (Exception)
            {
                retorno = false;
                throw;
            }
            return retorno;
        }
        public PedidosBD()
        {
            //
            // TODO: Adicionar lógica do construtor aqui
            //
        }
    }
}