﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using BetaPlast.App_Code.Classes;


namespace BetaPlast.App_Code.Persistencia
{
    public class ClienteBD
    {
        public bool Insert(Cliente cliente)
        {
            bool retorno = true;
            try
            {
                System.Data.IDbCommand objCommand;
                System.Data.IDbConnection objConnection;
                string sql = "";

                if (cliente.Tipo == "Pessoa")
                {
                    sql = "insert into tbl_cliente (cli_nome, cli_endereco, cli_telefone, cli_email, cli_tipo, cli_cpf) values (?nome, ?endereco, ?telefone, ?email, ?tipo, ?cpf);";
                    objConnection = Mapped.Connection();

                    objCommand = Mapped.Command(sql, objConnection);
                    objCommand.Parameters.Add(Mapped.Parameter("?nome", cliente.Nome));
                    objCommand.Parameters.Add(Mapped.Parameter("?endereco", cliente.Endereco));
                    objCommand.Parameters.Add(Mapped.Parameter("?telefone", cliente.Telefone));
                    objCommand.Parameters.Add(Mapped.Parameter("?email", cliente.Email));
                    objCommand.Parameters.Add(Mapped.Parameter("?tipo", cliente.Tipo));
                    objCommand.Parameters.Add(Mapped.Parameter("?cpf", cliente.CPF));
                    objCommand.ExecuteNonQuery();

                    objConnection.Close();
                    objConnection.Dispose();
                    objCommand.Dispose();

                }
                else if(cliente.Tipo == "Empresa")
                {
                    sql = "insert into tbl_cliente (cli_nome, cli_endereco, cli_telefone, cli_email, cli_tipo, cli_cnpj) values (?nome, ?endereco, ?telefone, ?email, ?tipo, ?cnpj);";
                    objConnection = Mapped.Connection();

                    objCommand = Mapped.Command(sql, objConnection);
                    objCommand.Parameters.Add(Mapped.Parameter("?nome", cliente.Nome));
                    objCommand.Parameters.Add(Mapped.Parameter("?endereco", cliente.Endereco));
                    objCommand.Parameters.Add(Mapped.Parameter("?telefone", cliente.Telefone));
                    objCommand.Parameters.Add(Mapped.Parameter("?email", cliente.Email));
                    objCommand.Parameters.Add(Mapped.Parameter("?tipo", cliente.Tipo));
                    objCommand.Parameters.Add(Mapped.Parameter("?cnpj", cliente.CNPJ));
                    objCommand.ExecuteNonQuery();

                    objConnection.Close();
                    objConnection.Dispose();
                    objCommand.Dispose();
                }

            }
            catch (Exception)
            {
                retorno = false;
                throw;
            }
            return retorno;
        }

        public Cliente Select(int id)
        {
            Cliente cliente = new Cliente();
            try
            {
                System.Data.IDbCommand objCommand;
                System.Data.IDbConnection objConnection;
                System.Data.IDataReader objDataReader;

                objConnection = Mapped.Connection();
                objCommand = Mapped.Command("select * from tbl_cliente where cli_codigo = ?id;", objConnection);
                objCommand.Parameters.Add(Mapped.Parameter("?id", id));

                objDataReader = objCommand.ExecuteReader();
                while (objDataReader.Read())
                {
                    cliente.Codigo = Convert.ToInt32(objDataReader["cli_codigo"]);
                    cliente.Nome = Convert.ToString(objDataReader["cli_nome"]);
                    cliente.Endereco = Convert.ToString(objDataReader["cli_endereco"]);
                    cliente.Telefone = Convert.ToString(objDataReader["cli_telefone"]);
                    cliente.Email = Convert.ToString(objDataReader["cli_email"]);
                    cliente.Tipo = Convert.ToString(objDataReader["cli_tipo"]);
                    cliente.CPF = Convert.ToString(objDataReader["cli_cpf"]);
                    cliente.CNPJ = Convert.ToString(objDataReader["cli_cnpj"]);
                    cliente.status = Convert.ToBoolean(objDataReader["cli_status"]);
                }
            }
            catch (Exception)
            {
                throw;
            }

            return cliente;
        }

        public DataSet SelectAll()
        {
            DataSet ds = new DataSet();

            try
            {
                System.Data.IDbCommand objCommand;
                System.Data.IDbConnection objConnection;
                System.Data.IDataAdapter objDataAdapter;

                objConnection = Mapped.Connection();
                objCommand = Mapped.Command("select * from tbl_cliente", objConnection);

                objDataAdapter = Mapped.Adapter(objCommand);
                objDataAdapter.Fill(ds);

                objCommand.Dispose();
                objConnection.Close();
                objConnection.Dispose();


            }
            catch (Exception)
            {
                throw;
            }

            return ds;
        }

        public bool Desativar(int codigo)
        {
            bool retorno = true;
            try
            {
                System.Data.IDbCommand objCommand;
                System.Data.IDbConnection objConnection;

                string sql = "update tbl_cliente set cli_status = false where cli_codigo =?codigo;";

                objConnection = Mapped.Connection();
                objCommand = Mapped.Command(sql, objConnection);

                objCommand.Parameters.Add(Mapped.Parameter("?codigo", codigo));


                objCommand.ExecuteNonQuery();

                objCommand.Dispose();
                objConnection.Close();
                objConnection.Dispose();
            }
            catch (Exception)
            {
                retorno = false;
                throw;
            }
            return retorno;
        }

        public bool Update(Cliente cliente)
        {
            bool retorno = true;
            try
            {
                System.Data.IDbCommand objCommand;
                System.Data.IDbConnection objConnection;
                string sql = "";

                if (cliente.Tipo == "Pessoa")
                {
                    sql = "update tbl_cliente set cli_nome=?nome, cli_endereco =?endereco, cli_telefone=?telefone, cli_email=?email, cli_tipo=?tipo, cli_cpf=?cpf where cli_codigo =?codigo ";
                    objConnection = Mapped.Connection();

                    objCommand = Mapped.Command(sql, objConnection);
                    objCommand.Parameters.Add(Mapped.Parameter("?nome", cliente.Nome));
                    objCommand.Parameters.Add(Mapped.Parameter("?endereco", cliente.Endereco));
                    objCommand.Parameters.Add(Mapped.Parameter("?telefone", cliente.Telefone));
                    objCommand.Parameters.Add(Mapped.Parameter("?email", cliente.Email));
                    objCommand.Parameters.Add(Mapped.Parameter("?tipo", cliente.Tipo));
                    objCommand.Parameters.Add(Mapped.Parameter("?cpf", cliente.CPF));
                    objCommand.Parameters.Add(Mapped.Parameter("?codigo", cliente.Codigo));
                    objCommand.ExecuteNonQuery();

                    objConnection.Close();
                    objConnection.Dispose();
                    objCommand.Dispose();

                }
                else if (cliente.Tipo == "Empresa")
                {
                    sql = "update tbl_cliente set cli_nome=?nome, cli_endereco =?endereco, cli_telefone=?telefone, cli_email=?email, cli_tipo=?tipo, cli_cnpj=?cnpj where cli_codigo =?codigo ";
                    objConnection = Mapped.Connection();

                    objCommand = Mapped.Command(sql, objConnection);
                    objCommand.Parameters.Add(Mapped.Parameter("?nome", cliente.Nome));
                    objCommand.Parameters.Add(Mapped.Parameter("?endereco", cliente.Endereco));
                    objCommand.Parameters.Add(Mapped.Parameter("?telefone", cliente.Telefone));
                    objCommand.Parameters.Add(Mapped.Parameter("?email", cliente.Email));
                    objCommand.Parameters.Add(Mapped.Parameter("?tipo", cliente.Tipo));
                    objCommand.Parameters.Add(Mapped.Parameter("?cnpj", cliente.CPF));
                    objCommand.Parameters.Add(Mapped.Parameter("?codigo", cliente.Codigo));
                    objCommand.ExecuteNonQuery();

                    objConnection.Close();
                    objConnection.Dispose();
                    objCommand.Dispose();
                }

            }
            catch (Exception)
            {
                retorno = false;
                throw;
            }
            return retorno;
        }

        public Cliente Autentica(string email)
        {
            Cliente obj = null;
            System.Data.IDbConnection objConexao;
            System.Data.IDbCommand objCommand;
            System.Data.IDataReader objDataReader;
            objConexao = Mapped.Connection();
            objCommand = Mapped.Command("SELECT * FROM tbl_cliente WHERE cli_email = ?email", objConexao);
           
            objCommand.Parameters.Add(Mapped.Parameter("?email", email));
            objDataReader = objCommand.ExecuteReader();
           
            while (objDataReader.Read())
            {
                obj = new Cliente();
                obj.Email = Convert.ToString(objDataReader["cli_email"]);
            }
            objDataReader.Close();
            objConexao.Close();
            objCommand.Dispose();
            objConexao.Dispose();
            objDataReader.Dispose();
            return obj;
         }

        public Cliente VerificarEmail(string Email)
        {
            Cliente cliente = null;

            System.Data.IDbCommand objCommand;
            System.Data.IDbConnection objConnection;
            System.Data.IDataReader objDataReader;


            objConnection = Mapped.Connection();
            objCommand = Mapped.Command("select * from tbl_cliente where cli_email = ?email", objConnection);
            objCommand.Parameters.Add(Mapped.Parameter("?email", Email));

            objDataReader = objCommand.ExecuteReader();
            while (objDataReader.Read())
            {
                ClienteBD clientebd = new ClienteBD();

                cliente = new Cliente();

                cliente.Email = Convert.ToString(objDataReader["cli_email"]);
            }

            objConnection.Close();
            objConnection.Dispose();
            objCommand.Dispose();


            return cliente;
        }

        public Cliente VerificarCpfCnpg(Cliente cliente)
        {
            Cliente obj = null;

            System.Data.IDbCommand objCommand;
            System.Data.IDbConnection objConnection;
            System.Data.IDataReader objDataReader;

            string sql = "";
            if(cliente.Tipo == "Pessoa")
            {
                sql = "select * from tbl_cliente where cli_cpf = ?dado";
                objConnection = Mapped.Connection();
                objCommand = Mapped.Command(sql, objConnection);
                objCommand.Parameters.Add(Mapped.Parameter("?dado", cliente.CPF));

                objDataReader = objCommand.ExecuteReader();
                while (objDataReader.Read())
                {
                    ClienteBD clientebd = new ClienteBD();

                    obj = new Cliente();

                    obj.Email = Convert.ToString(objDataReader["cli_cpf"]);
                }

                objConnection.Close();
                objConnection.Dispose();
                objCommand.Dispose();

            } else if (cliente.Tipo == "Empresa")
            {
                sql = "select * from tbl_cliente where cli_cnpj = ?dado";
                objConnection = Mapped.Connection();
                objCommand = Mapped.Command(sql, objConnection);
                objCommand.Parameters.Add(Mapped.Parameter("?dado", cliente.CNPJ));

                objDataReader = objCommand.ExecuteReader();
                while (objDataReader.Read())
                {
                    ClienteBD clientebd = new ClienteBD();

                    obj = new Cliente();

                    obj.Email = Convert.ToString(objDataReader["cli_cnpj"]);
                }

                objConnection.Close();
                objConnection.Dispose();
                objCommand.Dispose();
            }

            return obj;
        }
        public ClienteBD()
        {
            //
            // TODO: Adicionar lógica do construtor aqui
            //
        }
    }
}