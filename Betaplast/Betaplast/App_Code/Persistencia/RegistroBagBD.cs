﻿using BetaPlast.App_Code.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BetaPlast.App_Code.Persistencia
{
    public class RegistroBagBD
    {
        public bool Registrar(RegistroBag bag)
        {
            bool retorno = true;
            try
            {
                System.Data.IDbCommand objCommand;
                System.Data.IDbConnection objConnection;

                string insert = "insert into tbl_registrobag (bag_dataproducao, mem_codigo, map_codigo) values (?data, ?membro, ?materia);";

                objConnection = Mapped.Connection();
                objCommand = Mapped.Command(insert, objConnection);

                objCommand.Parameters.Add(Mapped.Parameter("?data", bag.DataProducao));
                objCommand.Parameters.Add(Mapped.Parameter("?membro", bag.Membro.Codigo));
                objCommand.Parameters.Add(Mapped.Parameter("?materia", bag.MateriaPrima.Codigo));
                objCommand.ExecuteNonQuery();

                objConnection.Close();
                objConnection.Dispose();
                objCommand.Dispose();


            }
            catch (Exception)
            {
                retorno = false;
                throw;
            }

            return retorno;
        }

        public RegistroBagBD()
        {
            //
            // TODO: Adicionar lógica do construtor aqui
            //
        }
    }
}