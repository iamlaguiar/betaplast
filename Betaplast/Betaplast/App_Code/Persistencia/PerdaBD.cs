﻿using System;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using BetaPlast.App_Code.Classes;

/// <summary>
/// Descrição resumida de PerdaBD
/// </summary>
namespace BetaPlast.App_Code.Persistencia
{
    public class PerdaBD
    {
        public bool Insert(Perda perda)
        {
            bool retorno = true;
            try
            {
                System.Data.IDbCommand objCommand;
                System.Data.IDbConnection objConnection;
                string sql = "";

                if (perda.Tipo == "Produto")
                {
                    sql = "insert into tbl_perda (per_kg, per_quantidade, per_motivo, per_data, per_tipo, pro_codigo) values (?kg, ?quantidade, ?motivo, ?data, ?tipo, ?produto);";
                    objConnection = Mapped.Connection();

                    objCommand = Mapped.Command(sql, objConnection);
                    objCommand.Parameters.Add(Mapped.Parameter("?kg", perda.KGPerdidos));
                    objCommand.Parameters.Add(Mapped.Parameter("?quantidade", perda.QuantidadePerdida));
                    objCommand.Parameters.Add(Mapped.Parameter("?motivo", perda.MotivoPerda));
                    objCommand.Parameters.Add(Mapped.Parameter("?data", perda.Data));
                    objCommand.Parameters.Add(Mapped.Parameter("?tipo", perda.Tipo));
                    objCommand.Parameters.Add(Mapped.Parameter("?produto", perda.Produto.Codigo));
                    objCommand.ExecuteNonQuery();

                    objConnection.Close();
                    objConnection.Dispose();
                    objCommand.Dispose();

                }
                else if (perda.Tipo == "Matéria-prima")
                {
                    sql = "insert into tbl_perda (per_kg, per_quantidade, per_motivo, per_data, per_tipo, map_codigo) values (?kg, ?quantidade, ?motivo, ?data, ?tipo, ?materia);";
                    objConnection = Mapped.Connection();

                    objCommand = Mapped.Command(sql, objConnection);
                    objCommand.Parameters.Add(Mapped.Parameter("?kg", perda.KGPerdidos));
                    objCommand.Parameters.Add(Mapped.Parameter("?quantidade", perda.QuantidadePerdida));
                    objCommand.Parameters.Add(Mapped.Parameter("?motivo", perda.MotivoPerda));
                    objCommand.Parameters.Add(Mapped.Parameter("?data", perda.Data));
                    objCommand.Parameters.Add(Mapped.Parameter("?tipo", perda.Tipo));
                    objCommand.Parameters.Add(Mapped.Parameter("?materia", perda.MateriaPrima.Codigo));
                    objCommand.ExecuteNonQuery();

                    objConnection.Close();
                    objConnection.Dispose();
                    objCommand.Dispose();
                }

            }
            catch (Exception)
            {
                retorno = false;
                throw;
            }
            return retorno;
        }

        public Perda Select(int id)
        {
            Perda perda = new Perda();
            try
            {
                System.Data.IDbCommand objCommand;
                System.Data.IDbConnection objConnection;
                System.Data.IDataReader objDataReader;

                objConnection = Mapped.Connection();
                objCommand = Mapped.Command("select * from tbl_perda where per_codigo = ?id;", objConnection);
                objCommand.Parameters.Add(Mapped.Parameter("?id", id));

                objDataReader = objCommand.ExecuteReader();
                while (objDataReader.Read())
                {
                    perda.Codigo = Convert.ToInt32(objDataReader["per_codigo"]);
                    perda.KGPerdidos = Convert.ToDouble(objDataReader["per_kg"]);
                    perda.QuantidadePerdida = Convert.ToInt32(objDataReader["per_quantidade"]);
                    perda.MotivoPerda = Convert.ToString(objDataReader["per_motivo"]);
                }
            }
            catch (Exception)
            {
                throw;
            }

            return perda;
        }

        public DataSet SelectAllWithProduto()
        {
            DataSet ds = new DataSet();

            try
            {
                System.Data.IDbCommand objCommand;
                System.Data.IDbConnection objConnection;
                System.Data.IDataAdapter objDataAdapter;

                objConnection = Mapped.Connection();
                objCommand = Mapped.Command("select pro_descricao as 'Item perdido', per_kg as 'KGs perdidos', per_quantidade as 'Quantidade perdida', per_motivo as 'Motivo', per_data as 'Data' " +
                    "from tbl_perda inner join tbl_produto using (pro_codigo) where pro_codigo is not null", objConnection);

                objDataAdapter = Mapped.Adapter(objCommand);
                objDataAdapter.Fill(ds);

                objCommand.Dispose();
                objConnection.Close();
                objConnection.Dispose();
            }
            catch (Exception)
            {
                throw;
            }

            return ds;
        }


        public DataSet SelectAllWithMateria()
        {
            DataSet ds = new DataSet();

            try
            {
                System.Data.IDbCommand objCommand;
                System.Data.IDbConnection objConnection;
                System.Data.IDataAdapter objDataAdapter;

                objConnection = Mapped.Connection();
                objCommand = Mapped.Command("select map_nome as 'Item perdido', per_kg as 'KGs perdidos', per_quantidade as 'Quantidade perdida', per_motivo as 'Motivo', per_data as 'Data' " +
                    "from tbl_perda inner join tbl_materiaprima using (map_codigo) where map_codigo is not null", objConnection);

                objDataAdapter = Mapped.Adapter(objCommand);
                objDataAdapter.Fill(ds);

                objCommand.Dispose();
                objConnection.Close();
                objConnection.Dispose();
            }
            catch (Exception)
            {
                throw;
            }

            return ds;
        }

        public PerdaBD()
        {
            //
            // TODO: Adicionar lógica do construtor aqui
            //
        }
    }
}