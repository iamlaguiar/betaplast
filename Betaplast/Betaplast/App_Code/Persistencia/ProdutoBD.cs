﻿using System;
using System.Collections.Generic;
using System.Web;
using BetaPlast.App_Code.Classes;
using BetaPlast;
using System.Data;

namespace BetaPlast.App_Code.Persistencia
{
    public class ProdutoBD
    {
        public bool InsertProduto(Produto produto)
        {
            bool retorno = true;
            try
            {
                System.Data.IDbCommand objCommand;
                System.Data.IDbConnection objConnection;

                string insert = "insert into tbl_produto (pro_descricao,  pro_pesoporkg, pro_largura, pro_espessura, pro_quantidadeatual, pro_quantidademinima,  " +
                    "pro_medidafinal, pro_tipofilme,  pro_observacoes, pro_viscosidade, pro_raio, map_codigo) values (?descricao,   ?pesoporkg, ?largura, " +
                    "?espessura, ?quantidadeatual, ?quantidademinima, ?medidafinal, ?tipofilme,  ?observacoes, ?viscosidade, ?raio, ?materia);";

                objConnection = Mapped.Connection();
                objCommand = Mapped.Command(insert, objConnection);

                objCommand.Parameters.Add(Mapped.Parameter("?descricao", produto.DescricaoProduto));
                objCommand.Parameters.Add(Mapped.Parameter("?pesoporkg", produto.Peso));
                objCommand.Parameters.Add(Mapped.Parameter("?largura", produto.Largura));
                objCommand.Parameters.Add(Mapped.Parameter("?espessura", produto.Espessura));
                objCommand.Parameters.Add(Mapped.Parameter("?quantidadeatual", produto.QuantidadeAtual));
                objCommand.Parameters.Add(Mapped.Parameter("?quantidademinima", produto.QuantidadeMinima));
                objCommand.Parameters.Add(Mapped.Parameter("?medidafinal", produto.MedidaFinal));
                objCommand.Parameters.Add(Mapped.Parameter("?tipofilme", produto.TipoFilme));
                objCommand.Parameters.Add(Mapped.Parameter("?observacoes", produto.Observacoes));
                objCommand.Parameters.Add(Mapped.Parameter("?viscosidade", produto.Viscosidade));
                objCommand.Parameters.Add(Mapped.Parameter("?raio", produto.Raio));
                objCommand.Parameters.Add(Mapped.Parameter("?materia", produto.MateriaPrima.Codigo));


                objCommand.ExecuteNonQuery();

                objConnection.Close();
                objConnection.Dispose();
                objCommand.Dispose();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                retorno = false;
            }

            return retorno;
        }

        public Produto Select(int id)
        {
            Produto produto = new Produto();
            try
            {
                System.Data.IDbCommand objCommand;
                System.Data.IDbConnection objConnection;
                System.Data.IDataReader objDataReader;

                objConnection = Mapped.Connection();
                objCommand = Mapped.Command("select * from tbl_produto where pro_codigo = ?id;", objConnection);
                objCommand.Parameters.Add(Mapped.Parameter("?id", id));

                objDataReader = objCommand.ExecuteReader();
                while (objDataReader.Read())
                {
                    MateriaPrimaBD materiaprimabd = new MateriaPrimaBD();

                    produto.Codigo = Convert.ToInt32(objDataReader["pro_codigo"]);
                    produto.DescricaoProduto = Convert.ToString(objDataReader["pro_descricao"]);
                    produto.Observacoes = Convert.ToString(objDataReader["pro_observacoes"]);
                    produto.Peso = Convert.ToDouble(objDataReader["pro_pesoporkg"]);
                    produto.Largura = Convert.ToDouble(objDataReader["pro_largura"]);
                    produto.QuantidadeMinima = Convert.ToInt32(objDataReader["pro_quantidademinima"]);
                    produto.QuantidadeAtual = Convert.ToInt32(objDataReader["pro_quantidadeatual"]);
                    produto.MedidaFinal = Convert.ToString(objDataReader["pro_medidafinal"]);
                    produto.TipoFilme = Convert.ToString(objDataReader["pro_tipofilme"]);
                    produto.Espessura = Convert.ToDouble(objDataReader["pro_espessura"]);
                    produto.Raio = Convert.ToDouble(objDataReader["pro_raio"]);
                    produto.Viscosidade = Convert.ToDouble(objDataReader["pro_viscosidade"]);
                    produto.MateriaPrima = materiaprimabd.Select(Convert.ToInt32(objDataReader["map_codigo"]));
                }

                objConnection.Close();
                objConnection.Dispose();
                objCommand.Dispose();
            }
            catch (Exception ex)
            {
                throw;
            }

            return produto;
        }

        public DataSet SelectAll()
        {
            DataSet ds = new DataSet();
            try
            {
                System.Data.IDbCommand objCommand;
                System.Data.IDbConnection objConnection;
                System.Data.IDataAdapter objDataAdapter;

                objConnection = Mapped.Connection();
                objCommand = Mapped.Command("select * from tbl_produto  where pro_status = true;", objConnection);

                objDataAdapter = Mapped.Adapter(objCommand);
                objDataAdapter.Fill(ds);

                objConnection.Close();
                objConnection.Dispose();
                objCommand.Dispose();
            }
            catch (Exception)
            {

                throw;
            }

            return ds;
        }

        public bool Update(Produto produto)
        {
            bool retorno = true;
            try
            {
                System.Data.IDbCommand objCommand;
                System.Data.IDbConnection objConnection;

                string update = "UPDATE tbl_produto SET  pro_descricao=?descricao, pro_pesoporkg=?pesoporkg, pro_largura=?largura, pro_quantidadeatual=?quantidadeatual, " +
                    "pro_quantidademinima=?quantidademinima,  pro_medidafinal=?medidafinal, pro_tipofilme=?tipofilme,  pro_observacoes=?observacoes, pro_espessura=?espessura, " +
                    "pro_viscosidade=?viscosidade, pro_raio=?raio, map_codigo=?materia WHERE pro_codigo=?codigo;";

                objConnection = Mapped.Connection();
                objCommand = Mapped.Command(update, objConnection);

                objCommand.Parameters.Add(Mapped.Parameter("?descricao", produto.DescricaoProduto));
                objCommand.Parameters.Add(Mapped.Parameter("?pesoporkg", produto.Peso));
                objCommand.Parameters.Add(Mapped.Parameter("?largura", produto.Largura));
                objCommand.Parameters.Add(Mapped.Parameter("?quantidadeatual", produto.QuantidadeAtual));
                objCommand.Parameters.Add(Mapped.Parameter("?quantidademinima", produto.QuantidadeMinima));
                objCommand.Parameters.Add(Mapped.Parameter("?tipofilme", produto.TipoFilme));
                objCommand.Parameters.Add(Mapped.Parameter("?medidafinal", produto.MedidaFinal));
                objCommand.Parameters.Add(Mapped.Parameter("?observacoes", produto.Observacoes));
                objCommand.Parameters.Add(Mapped.Parameter("?espessura", produto.Espessura));
                objCommand.Parameters.Add(Mapped.Parameter("?viscosidade", produto.Viscosidade));
                objCommand.Parameters.Add(Mapped.Parameter("?raio", produto.Raio));
                objCommand.Parameters.Add(Mapped.Parameter("?materia", produto.MateriaPrima.Codigo));
                objCommand.Parameters.Add(Mapped.Parameter("?codigo", produto.Codigo));


                objCommand.ExecuteNonQuery();

                objConnection.Close();
                objConnection.Dispose();
                objCommand.Dispose();

            }
            catch (Exception ex)
            {
                retorno = false;
            }

            return retorno;
        }

        public bool Desativar(int id)
        {
            bool retorno = true;
            try
            {
                System.Data.IDbCommand objCommand;
                System.Data.IDbConnection objConnection;

                string update = "update tbl_produto set pro_status = false where pro_codigo = ?codigo";

                objConnection = Mapped.Connection();
                objCommand = Mapped.Command(update, objConnection);
                objCommand.Parameters.Add(Mapped.Parameter("?codigo", id));

                objCommand.ExecuteNonQuery();

                objConnection.Close();
                objConnection.Dispose();
                objCommand.Dispose();
            }
            catch (Exception)
            {
                retorno = false;
                throw;
            }

            return retorno;
        }

        public Produto VerificarProduto(string descricao, string observacoes, double espessura, double largura, double raio, double viscosidade)
        {
            Produto produto = null;

            System.Data.IDbCommand objCommand;
            System.Data.IDbConnection objConnection;
            System.Data.IDataReader objDataReader;


            objConnection = Mapped.Connection();
            objCommand = Mapped.Command("select * from tbl_produto where pro_descricao = ?descricao and pro_observacoes = ?observacoes and pro_espessura = ?espessura and" +
                " pro_largura= ?largura and pro_raio = ?raio and pro_viscosidade = ?viscosidade;", objConnection);
            objCommand.Parameters.Add(Mapped.Parameter("?descricao", descricao));
            objCommand.Parameters.Add(Mapped.Parameter("?observacoes", observacoes));
            objCommand.Parameters.Add(Mapped.Parameter("?espessura", espessura));
            objCommand.Parameters.Add(Mapped.Parameter("?largura", largura));
            objCommand.Parameters.Add(Mapped.Parameter("?raio", raio));
            objCommand.Parameters.Add(Mapped.Parameter("?viscosidade", viscosidade));

            objDataReader = objCommand.ExecuteReader();
            while (objDataReader.Read())
            {
                MateriaPrimaBD materiaprimabd = new MateriaPrimaBD();

                produto = new Produto() ;

                produto.DescricaoProduto = Convert.ToString(objDataReader["pro_descricao"]);
                produto.Observacoes = Convert.ToString(objDataReader["pro_observacoes"]);
                produto.Largura = Convert.ToDouble(objDataReader["pro_largura"]);
                produto.Espessura = Convert.ToDouble(objDataReader["pro_espessura"]);
                produto.Raio = Convert.ToDouble(objDataReader["pro_raio"]);
                produto.Viscosidade = Convert.ToDouble(objDataReader["pro_viscosidade"]);
                produto.MateriaPrima = materiaprimabd.Select(Convert.ToInt32(objDataReader["map_codigo"]));
            }

            objConnection.Close();
            objConnection.Dispose();
            objCommand.Dispose();


            return produto;
        }

        public ProdutoBD()
        {

        }
    }
}