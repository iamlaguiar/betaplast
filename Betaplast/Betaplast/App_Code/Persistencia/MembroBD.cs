﻿using System;
using System.Collections.Generic;
using System.Web;
using BetaPlast.App_Code.Classes;
using BetaPlast;

namespace BetaPlast.App_Code.Persistencia
{
    public class MembroBD
    {
        public Membro Autentica(string email, string senha)
        {
            Membro obj = null;
            System.Data.IDbConnection objConexao;
            System.Data.IDbCommand objCommand;
            System.Data.IDataReader objDataReader;
            objConexao = Mapped.Connection();
            objCommand = Mapped.Command("SELECT * FROM tbl_Membro WHERE mem_email=?email and mem_senha=?senha", objConexao);
           
            objCommand.Parameters.Add(Mapped.Parameter("?email", email));
            objCommand.Parameters.Add(Mapped.Parameter("?senha", senha));
            objDataReader = objCommand.ExecuteReader();
            while (objDataReader.Read())
            {
                obj = new Membro()
                {
                    Codigo = Convert.ToInt32(objDataReader["mem_codigo"]),
                    Nome = Convert.ToString(objDataReader["mem_nome"]),
                    Email = Convert.ToString(objDataReader["mem_email"]),
                    Tipo = Convert.ToInt32(objDataReader["mem_tipo"])
                };
            }
            objDataReader.Close();
            objConexao.Close();
            objCommand.Dispose();
            objConexao.Dispose();
            objDataReader.Dispose();
            return obj;
        }

        public Membro Select(int id)
        {
            Membro obj = null;
            System.Data.IDbConnection objConexao;
            System.Data.IDbCommand objCommand;
            System.Data.IDataReader objDataReader;
            objConexao = Mapped.Connection();
            objCommand = Mapped.Command("SELECT * FROM tbl_Membro WHERE mem_codigo=?codigo",
           objConexao);
            objCommand.Parameters.Add(Mapped.Parameter("?codigo", id));
            objDataReader = objCommand.ExecuteReader();
            while (objDataReader.Read())
            {
                obj = new Membro();
                obj.Codigo = Convert.ToInt32(objDataReader["mem_codigo"]);
                obj.Nome = Convert.ToString(objDataReader["mem_nome"]);
                obj.Email = Convert.ToString(objDataReader["mem_email"]);
                obj.Tipo = Convert.ToInt32(objDataReader["mem_tipo"]);
            }
            objDataReader.Close();
            objConexao.Close();
            objCommand.Dispose();
            objConexao.Dispose();
            objDataReader.Dispose();
            return obj;
        }
        public MembroBD()
        {
            //
            // TODO: Add constructor logic here
            //
        }
    }
}