﻿using BetaPlast.App_Code.Classes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace BetaPlast.App_Code.Persistencia
{
    public class FornecedorBD
    {
        public bool Insert(Fornecedor fornecedor)
        {
            bool retorno = true;
            try
            {
                System.Data.IDbCommand objCommand;
                System.Data.IDbConnection objConnection;

                string sql = "insert into tbl_fornecedor (for_nome, for_telefone, for_email, for_cnpj, for_endereco) values (?nome, ?telefone, ?email, ?cnpj, ?endereco);";

                objConnection = Mapped.Connection();
                objCommand = Mapped.Command(sql, objConnection);


                objCommand.Parameters.Add(Mapped.Parameter("?nome", fornecedor.Nome));
                objCommand.Parameters.Add(Mapped.Parameter("?telefone", fornecedor.Telefone));
                objCommand.Parameters.Add(Mapped.Parameter("?email", fornecedor.Email));
                objCommand.Parameters.Add(Mapped.Parameter("?cnpj", fornecedor.CNPJ));
                objCommand.Parameters.Add(Mapped.Parameter("?endereco", fornecedor.Endereco));
                objCommand.ExecuteNonQuery();


                objConnection.Close();
                objCommand.Dispose();
                objConnection.Dispose();

            }
            catch (Exception)
            {
                retorno = false;
                throw;
            }

            return retorno;
        }

        public Fornecedor SelectByID(int id)
        {
            Fornecedor fornecedor = new Fornecedor();
            try
            {

                System.Data.IDbCommand objCommand;
                System.Data.IDbConnection objConnection;
                System.Data.IDataReader objDataReader;

                objConnection = Mapped.Connection();
                objCommand = Mapped.Command("select * from tbl_fornecedor where for_codigo = ?id;", objConnection);
                objCommand.Parameters.Add(Mapped.Parameter("?id", id));

                objDataReader = objCommand.ExecuteReader();
                while (objDataReader.Read())
                {
                    fornecedor.CNPJ = Convert.ToString(objDataReader["for_cnpj"]);
                    fornecedor.Codigo = Convert.ToInt32(objDataReader["for_codigo"]);
                    fornecedor.Nome = Convert.ToString(objDataReader["for_nome"]);
                    fornecedor.Endereco = Convert.ToString(objDataReader["for_endereco"]);
                    fornecedor.Email = Convert.ToString(objDataReader["for_email"]);
                    fornecedor.Telefone = Convert.ToString(objDataReader["for_telefone"]);
                }
                objConnection.Close();
                objCommand.Dispose();
                objConnection.Dispose();
            }

            catch (Exception)
            {

                throw;
            }

            return fornecedor;
        }

        public DataSet SelectAll()
        {
            DataSet ds = new DataSet();

            try
            {
                System.Data.IDbCommand objCommand;
                System.Data.IDbConnection objConnection;
                System.Data.IDataAdapter objDataAdapter;

                objConnection = Mapped.Connection();
                objCommand = Mapped.Command("select * from tbl_fornecedor where for_status = true", objConnection);

                objDataAdapter = Mapped.Adapter(objCommand);
                objDataAdapter.Fill(ds);

                objCommand.Dispose();
                objConnection.Close();
                objConnection.Dispose();


            }
            catch (Exception)
            {
                throw;
            }

            return ds;
        }

        public bool Update(Fornecedor fornecedor)
        {
            bool retorno = true;
            try
            {
                System.Data.IDbCommand objCommand;
                System.Data.IDbConnection objConnection;

                string sql = "update tbl_fornecedor set for_nome=?nome, for_telefone=?telefone, for_email=?email, for_cnpj=?cnpj, for_endereco=?endereco where for_codigo =?codigo;";

                objConnection = Mapped.Connection();
                objCommand = Mapped.Command(sql, objConnection);

                objCommand.Parameters.Add(Mapped.Parameter("?nome", fornecedor.Nome));
                objCommand.Parameters.Add(Mapped.Parameter("?telefone", fornecedor.Telefone));
                objCommand.Parameters.Add(Mapped.Parameter("?email", fornecedor.Email));
                objCommand.Parameters.Add(Mapped.Parameter("?cnpj", fornecedor.CNPJ));
                objCommand.Parameters.Add(Mapped.Parameter("?endereco", fornecedor.Endereco));
                objCommand.Parameters.Add(Mapped.Parameter("?codigo", fornecedor.Codigo));

                objCommand.ExecuteNonQuery();

                objCommand.Dispose();
                objConnection.Close();
                objConnection.Dispose();

            }
            catch (Exception)
            {
                retorno = false;
                throw;
            }

            return retorno;
        }

        public bool Desativar(int codigo)
        {
            bool retorno = true;
            try
            {
                System.Data.IDbCommand objCommand;
                System.Data.IDbConnection objConnection;

                string sql = "update tbl_fornecedor set for_status = false where for_codigo =?codigo;";

                objConnection = Mapped.Connection();
                objCommand = Mapped.Command(sql, objConnection);

                objCommand.Parameters.Add(Mapped.Parameter("?codigo", codigo));


                objCommand.ExecuteNonQuery();

                objCommand.Dispose();
                objConnection.Close();
                objConnection.Dispose();
            }
            catch (Exception)
            {
                retorno = false;
                throw;
            }
            return retorno;
        }

        public Fornecedor VerificarFornecedor(string nome, string cnpj)
        {
            Fornecedor fornecedor = null;
            try
            {
                System.Data.IDbCommand objCommand;
                System.Data.IDbConnection objConnection;
                System.Data.IDataReader objDataReader;

                objConnection = Mapped.Connection();
                objCommand = Mapped.Command("select * from tbl_fornecedor where for_nome = ?nome and for_cnpj = ?cnpj;", objConnection);
                objCommand.Parameters.Add(Mapped.Parameter("?nome", nome));
                objCommand.Parameters.Add(Mapped.Parameter("?cnpj", cnpj));

                objDataReader = objCommand.ExecuteReader();
                while (objDataReader.Read())
                {
                    fornecedor = new Fornecedor();

                    fornecedor.CNPJ = Convert.ToString(objDataReader["for_cnpj"]);
                    fornecedor.Codigo = Convert.ToInt32(objDataReader["for_codigo"]);
                    fornecedor.Nome = Convert.ToString(objDataReader["for_nome"]);
                    fornecedor.Endereco = Convert.ToString(objDataReader["for_endereco"]);
                    fornecedor.Email = Convert.ToString(objDataReader["for_email"]);
                    fornecedor.Telefone = Convert.ToString(objDataReader["for_telefone"]);
                }


                objConnection.Close();
                objConnection.Dispose();
                objCommand.Dispose();
            }
            catch (Exception)
            {
                throw;
            }

            return fornecedor;
        }

        public FornecedorBD()
        {
            //
            // TODO: Adicionar lógica do construtor aqui
            //
        }
    }
}