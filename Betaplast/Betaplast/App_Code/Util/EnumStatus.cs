﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BetaPlast.Util
{
    public enum EnumStatus
    {
       Extrusora = 1,  
       Impressão = 2,
       Corte = 3,
       Estoque = 4,
       Enviando = 5,
       Finalizado = 6,
       Reprovado = 7,
       Entregue = 8
    }
}