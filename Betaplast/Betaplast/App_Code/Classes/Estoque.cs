﻿using System;
using System.Collections.Generic;
using System.Web;

namespace BetaPlast.App_Code.Classes
{
    public class Estoque
    {
        public int Codigo { get; set; }
        public double QuantidadeAtual { get; set; }
        public double QuantidadeMinima { get; set; }
        public double Peso { get; set; }
        public string Tipo { get; set; }

        public Estoque()
        {
   
        }
    }
}