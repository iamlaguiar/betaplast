﻿using System;
using System.Collections.Generic;
using System.Web;

namespace BetaPlast.App_Code.Classes
{
    public class Produto
    {
        public int Codigo { get; set; }
        public string DescricaoProduto { get; set; } 
        public double Largura { get; set; }
        public double Espessura { get; set; }
        public string MedidaFinal { get; set; }
        public string TipoFilme { get; set; }
        public string Observacoes { get; set; }
        public double QuantidadeAtual { get; set; }
        public double QuantidadeMinima { get; set; }
        public double Peso { get; set; }
        public double Viscosidade { get; set; }
        public double Raio { get; set; }
        public MateriaPrima MateriaPrima { get; set; }

        public Produto()
        {
        
        }
    }
}