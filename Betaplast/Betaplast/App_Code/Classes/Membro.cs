﻿using System;
using System.Collections.Generic;
using System.Web;

namespace BetaPlast.App_Code.Classes
{
    public class Membro
    {
        public int Codigo { get; set; }
        public string Nome { get; set; }
        public string Endereco { get; set; }
        public string Telefone { get; set; }
        public string Email { get; set; }
        public string Senha { get; set; }
        public string RG { get; set; }
        public string CPF { get; set; }
        public int Tipo { get; set; }
        public string Matricula { get; set; }

        public Membro()
        {

        }
    }
}