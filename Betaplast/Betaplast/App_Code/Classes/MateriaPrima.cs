﻿using System;
using System.Collections.Generic;
using System.Web;

namespace BetaPlast.App_Code.Classes
{
    public class MateriaPrima
    {
        public int Codigo { get; set; }
        public string Nome { get; set; }
        public double QuantidadeAtual { get; set; }
        public double QuantidadeMinima { get; set; }
        public double Peso { get; set; }    
        public string Cor { get; set; }

        public Fornecedor Fornecedor { get; set; } 

        public List<Fornecedor> Fornecedores { get; set; }

        public MateriaPrima()
        {

        }
    }
}