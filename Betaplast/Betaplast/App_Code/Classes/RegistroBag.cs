﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BetaPlast.App_Code.Classes
{
    public class RegistroBag
    {
        public DateTime DataProducao { get; set; }
        public MateriaPrima MateriaPrima { get; set; }
        public Membro Membro { get; set; }

        public RegistroBag()
        {
            //
            // TODO: Adicionar lógica do construtor aqui
            //
        }
    }
}