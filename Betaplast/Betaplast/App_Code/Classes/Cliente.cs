﻿using System;
using System.Collections.Generic;
using System.Web;

namespace BetaPlast.App_Code.Classes
{
    public class Cliente
    {
        public int Codigo { get; set; }
        public string Nome { get; set; }
        public string Endereco { get; set; }
        public string Telefone { get; set; }
        public string Email { get; set; }
        public string Tipo { get; set; }
        public string CPF { get; set; }
        public string CNPJ { get; set; }
        public bool status { get; set; }

        public Cliente()
        {
         
        }
    }
}