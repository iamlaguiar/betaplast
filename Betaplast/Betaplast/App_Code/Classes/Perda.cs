﻿using System;
using System.Collections.Generic;
using System.Web;

namespace BetaPlast.App_Code.Classes
{
    public class Perda
    {
        public int Codigo { get; set; }
        public double KGPerdidos { get; set; }
        public int QuantidadePerdida { get; set; }
        public string MotivoPerda { get; set; }
        public string Tipo { get; set; }
        public DateTime Data { get; set; }
        public Produto Produto { get; set; }
        public MateriaPrima MateriaPrima { get; set; }

        public Perda()
        {

        }
    }
}