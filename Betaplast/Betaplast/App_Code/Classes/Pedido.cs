﻿using System;
using System.Collections.Generic;
using System.Web;

namespace BetaPlast.App_Code.Classes
{
    public class Pedido
    {
        public int Codigo { get; set; }
        public string Descricao { get; set; }
        public int Quantidade { get; set; }
        public double Peso { get; set; }
        public DateTime DataPedido { get; set; }
        public DateTime DataEntrega { get; set; }
        public string Obersevacoes { get; set; }
        public string Status { get; set; }
        public Produto Produto { get; set; }
        public Cliente Cliente;

        public Pedido()
        {
            //
            // TODO: Adicionar lógica do construtor aqui
            //
        }
    }
}