﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ListarFornecedor.aspx.cs" MasterPageFile="~/Paginas/Master/Index.master" Inherits="Paginas_Fornecedor_ListarFornecedor" %>


<asp:Content ID="MenuLateral" ContentPlaceHolderID="MenuLateral" runat="Server">
    <script src="../../Scripts/jquery.dataTables.min.js"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            $('#<%=grdViewFornecedor.ClientID%>').dataTable();
        });
    </script>

</asp:Content>

<asp:Content ID="Conteudo" ContentPlaceHolderID="ConteudoPagina" runat="Server">
    <h3 class="page-header">Lista de Fornecedores</h3>

    <div id="main" runat="server" style="margin-left: 190px;" class="container">
        <div class="col-sm-9">
            <asp:Label ID="lblMensagem" runat="server"></asp:Label>
            <br />
            <asp:GridView ID="grdViewFornecedor" runat="server" AutoGenerateColumns="False" CssClass="display" OnRowCommand="grdViewFornecedor_RowCommand"
                  AllowPaging="true" GridLines="None" BorderStyle="none" HorizontalAlign="center">
                <Columns>
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:LinkButton ID="lbAlterar" runat="server" CommandName="Alterar" CommandArgument='<%# Bind("for_codigo")%>'>Alterar</asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:LinkButton ID="lbDeletar" runat="server" CommandName="Desativar" CommandArgument='<%# Bind("for_codigo")%>'>Desativar</asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="for_nome" HeaderText="Nome" />
                    <asp:BoundField DataField="for_endereco" HeaderText="Endereço" />
                    <asp:BoundField DataField="for_telefone" HeaderText="Telefone" />
                    <asp:BoundField DataField="for_email" HeaderText="Email" />
                    <asp:BoundField DataField="for_cnpj" HeaderText="CNPJ" />
                </Columns>
                <HeaderStyle BackColor="#0066ff" ForeColor="White" />
            </asp:GridView>
        </div>
    </div>
</asp:Content>
