﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="AlterarFornecedor.aspx.cs" MasterPageFile="~/Paginas/Master/Index.master" Inherits="Paginas_Fornecedor_AlterarFornecedor" %>

<asp:Content ID="MenuLateral" ContentPlaceHolderID="MenuLateral" runat="Server">
</asp:Content>
<asp:Content ID="Conteudo1" ContentPlaceHolderID="ConteudoPagina" runat="Server">
    <h3 class="page-header">Alteração de Fornecedor</h3>

    <div id="main" runat="server" style="margin-left: 200px;" class="container">
        <div class="col-sm-9">

            <div class="form-group col-md-6">
                <asp:Label ID="lblNome" runat="server" Text="Nome:"></asp:Label>
                <asp:TextBox ID="txtNome" CssClass="form-control" runat="server"></asp:TextBox>
            </div>
            <div class="form-group col-md-3">
                <asp:Label ID="lblCnpj" runat="server" Text="CNPJ:"></asp:Label>
                <asp:TextBox ID="txtCnpj" CssClass="form-control" runat="server"></asp:TextBox>
            </div>
            <div class="form-group col-md-3">
                <asp:Label ID="lblTelefone" runat="server" Text="Telefone:"></asp:Label>
                <asp:TextBox ID="txtTelefone" CssClass="form-control" runat="server"></asp:TextBox>
            </div>
            <div class="form-group col-md-6">
                <asp:Label ID="lblEmail" runat="server" Text="Email:"></asp:Label>
                <asp:TextBox ID="txtEmail" CssClass="form-control" runat="server"></asp:TextBox>
            </div>
            <div class="form-group col-md-6">
                <asp:Label ID="lblEndereco" runat="server" Text="Endereço:"></asp:Label>
                <asp:TextBox ID="txtEndereco" CssClass="form-control" runat="server"></asp:TextBox>
            </div>
            <div class="form-group col-md-4">
                <asp:Label ID="lblMensagem" runat="server" Text=""></asp:Label>
                <asp:Button ID="btnSalvar" class="btn btn-primary" runat="server" Text="Salvar" OnClick="btnSalvar_Click" />
                <asp:Button class="btn btn-danger" runat="server" Text="Voltar" ID="btnVoltar" OnClick="btnSair_Click" />
            </div>
        </div>
    </div>
</asp:Content>

