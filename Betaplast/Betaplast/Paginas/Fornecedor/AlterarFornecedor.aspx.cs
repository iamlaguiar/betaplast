﻿using BetaPlast.App_Code.Classes;
using BetaPlast.App_Code.Persistencia;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Paginas_Fornecedor_AlterarFornecedor : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            Carrega();
        }
    }

    public void Carrega()
    {
        FornecedorBD bd = new FornecedorBD();
        Fornecedor fornecedor = bd.SelectByID(Convert.ToInt32(Session["IDfornecedor"]));
        txtNome.Text = fornecedor.Nome;
        txtEndereco.Text = fornecedor.Endereco;
        txtTelefone.Text = fornecedor.Telefone;
        txtCnpj.Text = fornecedor.CNPJ;
        txtEmail.Text = fornecedor.Email;
    }

    protected void btnSalvar_Click(object sender, EventArgs e)
    {
        FornecedorBD bd = new FornecedorBD();
        Fornecedor fornecedor = bd.SelectByID(Convert.ToInt32(Session["IDfornecedor"]));

        fornecedor.Nome = txtNome.Text;
        fornecedor.Endereco = txtEndereco.Text;
        fornecedor.Email = txtEmail.Text;
        fornecedor.CNPJ = txtCnpj.Text;
        fornecedor.Telefone = txtTelefone.Text;

        lblMensagem.Text = bd.Update(fornecedor) ? "Dados atualizados com sucesso" : "Erro ao atualizar dados";

    }

    protected void btnSair_Click(object sender, EventArgs e)
    {
        Response.Redirect("ListarFornecedor.aspx");
    }
}