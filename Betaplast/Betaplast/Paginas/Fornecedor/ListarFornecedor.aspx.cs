﻿using BetaPlast.App_Code.Persistencia;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Paginas_Fornecedor_ListarFornecedor : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Carrega();
    }

    protected void Carrega()
    {
        FornecedorBD bd = new FornecedorBD();
        DataSet ds = bd.SelectAll();
        grdViewFornecedor.DataSource = ds.Tables[0].DefaultView;
        grdViewFornecedor.DataBind();
        grdViewFornecedor.HeaderRow.TableSection = TableRowSection.TableHeader;
    }

    protected void grdViewFornecedor_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        int codigo = 0;
        switch (e.CommandName)
        {
            case "Alterar":
                codigo = Convert.ToInt32(e.CommandArgument);
                Session["IDfornecedor"] = codigo;
                Response.Redirect("AlterarFornecedor.aspx");
                break;

            case "Desativar":
                FornecedorBD bd = new FornecedorBD();
                bd.Desativar(Convert.ToInt32(e.CommandArgument));
                Carrega();
                break;
        }
    }
}