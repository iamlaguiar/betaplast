﻿using BetaPlast.App_Code.Classes;
using BetaPlast.App_Code.Persistencia;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Paginas_Fornecedor_CadastrarFornecedor : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void LimparCampos()
    {
        txtNome.Text = "";
        txtEndereco.Text = "";;
        txtTelefone.Text = "";
        txtCnpj.Text = "";
        txtEmail.Text = "";
    }

    protected void btnSalvar_Click(object sender, EventArgs e)
    {
        Fornecedor fornecedor = new Fornecedor();
        fornecedor.Nome = txtNome.Text;
        fornecedor.Telefone = txtTelefone.Text;
        fornecedor.Email = txtEmail.Text;
        fornecedor.CNPJ = txtCnpj.Text;
        fornecedor.Endereco = txtEndereco.Text;

        if (!VerificaFornecedor(fornecedor))
        {
            FornecedorBD bd = new FornecedorBD();

            if (bd.Insert(fornecedor))
            {
                lblMensagem.Text = "Fornecedor cadastrado com sucesso";
                LimparCampos();
            }
            else
            {
                lblMensagem.Text = "Erro ao atualizar dados";
            }
        } else
        {
            lblMensagem.Text = "fornecedor ja cadastrado";
        }
    }

    protected bool VerificaFornecedor(Fornecedor fornecedor)
    {
        bool retorno = false;
        FornecedorBD bd = new FornecedorBD();
        if (bd.VerificarFornecedor(fornecedor.Nome, fornecedor.CNPJ) != null)
        {
            retorno = true;
        }
        return retorno;
    }


}