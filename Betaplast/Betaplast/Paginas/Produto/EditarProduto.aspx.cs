﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BetaPlast.App_Code.Classes;
using BetaPlast.App_Code.Persistencia;
using System.Data;

public partial class Paginas_Produto_EditarProduto : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            Carrega();
        }
    }


    public void Carrega()
    {
        ProdutoBD bd = new ProdutoBD();
        Produto produto = bd.Select(Convert.ToInt32(Session["IDproduto"]));

        txtDescricaoProduto.Text = produto.DescricaoProduto;
        txtPesoPorKGProduto.Text = produto.Peso.ToString();
        txtLarguraProduto.Text = produto.Largura.ToString();
        txtEspessuraProduto.Text = produto.Espessura.ToString();
        txtQuantidadeAtual.Text = produto.QuantidadeAtual.ToString();
        txtQuantidadeMinima.Text = produto.QuantidadeMinima.ToString();
        txtMedidaFinal.Text = produto.MedidaFinal;
        txtTipoFilme.Text = produto.TipoFilme;
        txtObservacoes.Text = produto.Observacoes;
        txtViscosidade.Text = produto.Viscosidade.ToString();
        txtRaio.Text = produto.Raio.ToString();

        MateriaPrimaBD materiaprimabd = new MateriaPrimaBD();
        DataSet clienteds = materiaprimabd.SelectAll();

        ddlMateriaPrima.DataSource = clienteds.Tables[0].DefaultView;
        ddlMateriaPrima.DataTextField = "map_nome";
        ddlMateriaPrima.DataValueField = "map_codigo";
        ddlMateriaPrima.DataBind();
        //ddlMateriaPrima.Items.Insert(0, produto.MateriaPrima.Nome.ToString());
        ddlMateriaPrima.SelectedIndex = produto.MateriaPrima.Codigo;
    }

    protected void btnEditar_Click(object sender, EventArgs e)
    {
        try
        {
            MateriaPrimaBD materiaPrimaBD = new MateriaPrimaBD();
            MateriaPrima materiaPrima = materiaPrimaBD.Select(Convert.ToInt32(ddlMateriaPrima.SelectedItem.Value));
            ProdutoBD bd = new ProdutoBD();

            Produto produto = bd.Select(Convert.ToInt32(Session["IDproduto"]));

            produto.DescricaoProduto = txtDescricaoProduto.Text.ToString();
            produto.Peso = Convert.ToDouble(txtPesoPorKGProduto.Text);
            produto.Largura = Convert.ToDouble(txtLarguraProduto.Text);
            produto.Espessura = Convert.ToDouble(txtEspessuraProduto.Text);
            produto.QuantidadeMinima = Convert.ToDouble(txtQuantidadeMinima.Text);
            produto.QuantidadeAtual = Convert.ToDouble(txtQuantidadeAtual.Text);
            produto.MedidaFinal = txtMedidaFinal.Text.ToString();
            produto.TipoFilme = txtTipoFilme.Text.ToString();
            produto.Observacoes = txtObservacoes.Text.ToString();
            produto.MateriaPrima = materiaPrima;
            produto.Raio = Convert.ToDouble(txtRaio.Text);
            produto.Viscosidade = Convert.ToDouble(txtViscosidade.Text);

            lblMensagem.Text = bd.Update(produto) ? "Produto editado com sucesso" : "Erro ao editar produto";


        }
        catch (Exception ex)
        {
            lblMensagem.Text = "" + ex;
        }
    }
}