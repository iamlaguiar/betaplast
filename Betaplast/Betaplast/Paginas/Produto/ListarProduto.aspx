﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ListarProduto.aspx.cs" MasterPageFile="~/Paginas/Master/Index.master" Inherits="Paginas_Produto_ListarProduto" %>


<asp:Content ID="MenuLateral" ContentPlaceHolderID="MenuLateral" runat="Server">
    <script src="../../Scripts/jquery.dataTables.min.js"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            $('#<%=grdViewProduto.ClientID%>').dataTable();
        });
    </script>

</asp:Content>

<asp:Content ID="Conteudo" ContentPlaceHolderID="ConteudoPagina" runat="Server">
    <h3 class="page-header">Lista de Produtos</h3>

    <div id="main" runat="server" style="margin-left: 190px;" class="container">
        <div class="col-sm-9">
            <asp:Label ID="lblMensagem" runat="server"></asp:Label>
            <br />
            <asp:GridView ID="grdViewProduto" runat="server" AutoGenerateColumns="False" OnRowCommand="grdViewProduto_RowCommand"
                AllowPaging="true" CssClass="display" GridLines="None" BorderStyle="none" HorizontalAlign="center">
                <Columns>
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:LinkButton ID="lbAlterar" runat="server" CommandName="Alterar" CommandArgument='<%# Bind("pro_codigo")%>'>Alterar</asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:LinkButton ID="lbDeletar" runat="server" CommandName="Desativar" CommandArgument='<%# Bind("pro_codigo")%>'>Desativar</asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="pro_descricao" HeaderText="Descrição" />
                    <asp:BoundField DataField="pro_quantidadeatual" HeaderText="Quantidade atual" />
                    <asp:BoundField DataField="pro_quantidademinima" HeaderText="Quantidade miníma" />
                    <asp:BoundField DataField="pro_raio" HeaderText="Raio" />
                    <asp:BoundField DataField="pro_viscosidade" HeaderText="Viscosidade" />
                    <asp:BoundField DataField="pro_pesoporkg" HeaderText="Peso(KG)" />
                </Columns>
                <HeaderStyle BackColor="#0066ff" ForeColor="White" />
            </asp:GridView>
        </div>
    </div>
</asp:Content>
