﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BetaPlast.App_Code.Classes;
using BetaPlast.App_Code.Persistencia;
using System.Data;

public partial class Paginas_Produto_CadastrarProduto : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            MateriaPrimaBD materiaprimabd = new MateriaPrimaBD();
            DataSet clienteds = materiaprimabd.SelectAll();

            ddlMateriaPrima.DataSource = clienteds.Tables[0].DefaultView;
            ddlMateriaPrima.DataTextField = "map_nome";
            ddlMateriaPrima.DataValueField = "map_codigo";
            ddlMateriaPrima.DataBind();
            ddlMateriaPrima.Items.Insert(0, "Selecione");
        }
    }

    protected void BtnCadastrar_Click(object sender, EventArgs e)
    {

        MateriaPrimaBD materiaPrimaBD = new MateriaPrimaBD();

        Produto produto = new Produto();

        produto.DescricaoProduto = txtDescricaoProduto.Text;
        produto.Peso = Convert.ToDouble(txtPesoPorKGProduto.Text);
        produto.Largura = Convert.ToDouble(txtLarguraProduto.Text);
        produto.Espessura = Convert.ToDouble(txtEspessuraProduto.Text);
        produto.QuantidadeMinima = Convert.ToDouble(txtQuantidadeMinima.Text);
        produto.QuantidadeAtual = Convert.ToDouble(txtQuantidadeAtual.Text);
        produto.MedidaFinal = txtMedidaFinal.Text;
        produto.TipoFilme = txtTipoFilme.Text;
        produto.Observacoes = txtObservacoes.Text;
        produto.Raio = Convert.ToDouble(txtRaio.Text);
        produto.Viscosidade = Convert.ToDouble(txtViscosidade.Text);
        produto.MateriaPrima = materiaPrimaBD.Select(Convert.ToInt32(ddlMateriaPrima.SelectedItem.Value));


        ProdutoBD produtobd = new ProdutoBD();
        if (!VerificaPoduto(produto))
        {
            if (produtobd.InsertProduto(produto))
            {
                lblMensagem.Text = "Produto Cadastrado com sucesso";
                LimparCampos();
            }
            else
            {
                lblMensagem.Text = "Erro a cadastrar produto";
            }
        }
        else
        {
            lblMensagem.Text = "Produto ja cadastrado";
        }
    }

   protected void LimparCampos()
    {
        txtDescricaoProduto.Text = "";
        txtPesoPorKGProduto.Text = "";
        txtLarguraProduto.Text = "";
        txtEspessuraProduto.Text = "";
        txtQuantidadeAtual.Text = "";
        txtQuantidadeMinima.Text = "";
        txtMedidaFinal.Text = "";
        txtTipoFilme.Text = "";
        txtRaio.Text = "";
        txtViscosidade.Text = "";
        txtObservacoes.Text = "";
    }

    protected bool VerificaPoduto(Produto produto)
    {
        bool retorno = false;
        ProdutoBD produtobd = new ProdutoBD();
        if(produtobd.VerificarProduto(produto.DescricaoProduto, produto.Observacoes, produto.Espessura, produto.Largura, produto.Raio, produto.Viscosidade) != null)
        {
            retorno = true;
        }
        return retorno;
    }
}