﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="CadastrarProduto.aspx.cs" MasterPageFile="~/Paginas/Master/Index.master" Inherits="Paginas_Produto_CadastrarProduto" %>

<asp:Content ID="MenuLateral" ContentPlaceHolderID="MenuLateral" runat="Server">
</asp:Content>


<asp:Content ID="Conteudo" ContentPlaceHolderID="ConteudoPagina" runat="Server">
    <h3 class="page-header">Cadastro de Produto</h3>

    <div id="main" runat="server" style="margin-left: 190px;" class="container">
        <div class="col-sm-9">
            <div class="row">
                <div class="row">
                    <div class="form-group col-md-3">
                        <asp:Label ID="lblDescricaoProduto" runat="server" Text="Descrição do produto"></asp:Label>
                        <asp:TextBox ID="txtDescricaoProduto" class="form-control" runat="server"></asp:TextBox>
                    </div>
                    <div class="form-group col-md-3">
                        <asp:Label ID="lblPesoPorKGProduto" runat="server" Text="Peso por KG"></asp:Label>
                        <asp:TextBox ID="txtPesoPorKGProduto" class="form-control" runat="server"></asp:TextBox>
                    </div>
                    <div class="form-group col-md-3">
                        <asp:Label ID="lblLarguraProduto" runat="server" Text="Largura do produto"></asp:Label>
                        <asp:TextBox ID="txtLarguraProduto" class="form-control" runat="server"></asp:TextBox>
                    </div>
                    <div class="form-group col-md-3">
                        <asp:Label ID="lblEspessuraProduto" runat="server" Text="Espessura do produto"></asp:Label>
                        <asp:TextBox ID="txtEspessuraProduto" class="form-control" runat="server"></asp:TextBox>
                    </div>

                </div>
                <div class="row">
                    <div class="form-group col-md-3">
                        <asp:Label ID="lblMedidaFinal" runat="server" Text="Medidade final do produto"></asp:Label>
                        <asp:TextBox ID="txtMedidaFinal" class="form-control" runat="server"></asp:TextBox>
                    </div>
                    <div class="form-group col-md-3">
                        <asp:Label ID="lblTipoFilme" runat="server" Text="Tipo do filme"></asp:Label>
                        <asp:TextBox ID="txtTipoFilme" class="form-control" runat="server"></asp:TextBox>
                    </div>
                    <div class="form-group col-md-3">
                        <asp:Label ID="lblMateriaPrima" runat="server" Text="Materia prima utilizada"></asp:Label>
                        <br />
                        <asp:DropDownList ID="ddlMateriaPrima" CssClass="form-control" runat="server"></asp:DropDownList>
                    </div>
                    <div class="form-group col-md-3">
                        <asp:Label ID="lblRaio" runat="server" Text="Raio"></asp:Label>
                        <asp:TextBox ID="txtRaio" class="form-control" runat="server"></asp:TextBox>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-md-3">
                        <asp:Label ID="lblQuantidadeMinima" runat="server" Text="Quantidade mínima"></asp:Label>
                        <asp:TextBox ID="txtQuantidadeMinima" class="form-control" runat="server"></asp:TextBox>
                    </div>
                    <div class="form-group col-md-3">
                        <asp:Label ID="lblQuantidadeAtual" runat="server" Text="Quantidade atual"></asp:Label>
                        <asp:TextBox ID="txtQuantidadeAtual" class="form-control" runat="server"></asp:TextBox>
                    </div>

                    <div class="form-group col-md-3">
                        <asp:Label ID="lblViscosidade" runat="server" Text="Viscosidade"></asp:Label>
                        <asp:TextBox ID="txtViscosidade" class="form-control" runat="server"></asp:TextBox>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-md-6">
                        <asp:Label ID="lblObservacoes" runat="server" Text="Observações"></asp:Label>
                        <asp:TextBox ID="txtObservacoes" TextMod="multiline" class="form-control" runat="server"></asp:TextBox>
                    </div>
                </div>
                <hr>
                <div id="actions" class="row">
                    <div class="col-md-4">
                        <asp:Button ID="btnCadastrar" class="btn btn-primary" runat="server" Text="Cadastrar" OnClick="BtnCadastrar_Click" />
                        <%--<asp:Button class="btn btn-danger" runat="server" Text="Limpar campos" ID="btnLimparCampos" OnClick="btnLimparCampos_Click"/>--%>
                        <asp:Label ID="lblMensagem" runat="server" Text=""></asp:Label>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
