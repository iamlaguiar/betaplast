﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BetaPlast.App_Code.Classes;
using BetaPlast.App_Code.Persistencia;
using System.Data;

public partial class Paginas_Produto_ListarProduto : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Carrega();
        Check();
    }

    protected void grdViewProduto_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        int codigo = 0;
        switch (e.CommandName)
        {
            case "Alterar":
                codigo = Convert.ToInt32(e.CommandArgument);
                Session["IDproduto"] = codigo;
                Response.Redirect("EditarProduto.aspx");
                break;

            case "Desativar":
                ProdutoBD bd = new ProdutoBD();
                bd.Desativar(Convert.ToInt32(e.CommandArgument));
                Carrega();
                break;
        }
    }

    protected void Carrega()
    {
        ProdutoBD bd = new ProdutoBD();
        DataSet ds = bd.SelectAll();
        grdViewProduto.DataSource = ds.Tables[0].DefaultView;
        grdViewProduto.DataBind();
        grdViewProduto.HeaderRow.TableSection = TableRowSection.TableHeader;
    }

    protected void Check()
    {
        foreach (GridViewRow row in grdViewProduto.Rows)
        {
            if (Convert.ToInt32(row.Cells[3].Text) < Convert.ToInt32(row.Cells[4].Text))
            {
                lblMensagem.Text = "Existem produtos com o estoque abaixo do valor mínimo";
                for(int i = 2; i < grdViewProduto.Columns.Count; i ++)
                row.Cells[i].CssClass = "btn-danger";
            }
        }
    }
}