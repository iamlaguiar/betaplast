﻿using BetaPlast.App_Code.Persistencia;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Paginas_MateriaPrima_Grafico : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            GraficoMateria();
        }
    }


    private void GraficoMateria()
    {
        StringBuilder strBuilder = new StringBuilder();
        DataSet materiaprimads = new DataSet();
        MateriaPrimaBD materiaprimabd = new MateriaPrimaBD();

        try
        {
            materiaprimads = materiaprimabd.SelectAll();

            strBuilder.Append(@"<script type='text/javascript'>
                                    google.charts.load('current', {packages: ['corechart']});

                                    google.charts.setOnLoadCallback(drawVisualization);

                                    function drawVisualization(){
                                    var data = new google.visualization.DataTable();");
            strBuilder.Append(@"
                                    data.addColumn('string', 'topping');
                                    data.addColumn('number', 'Quantidade');
                                    data.addColumn('number', 'Quantidade mínima');");

            strBuilder.Append(@"data.addRows([");

            for (int i = 0; i < materiaprimads.Tables[0].Rows.Count; i++)
            {
                strBuilder.Append(@"['" + materiaprimads.Tables[0].Rows[i]["map_nome"] + "', " + materiaprimads.Tables[0].Rows[i]["map_quantidadeatual"] + ", " + materiaprimads.Tables[0].Rows[i]["map_quantidademinima"] + "],");
            }
            strBuilder.Remove(strBuilder.Length - 1, 1);
            strBuilder.Append(@"]);");

            strBuilder.Append(@"
                                    var options = {'title': 'Matéria prima em estoque',
                                    'width': 1000,
                                    'height': 400,
                                     'bar': {groupWidth: '100%', width: 20},
                                     'chartArea': {'width':'80%', left:50, height: '50%'}}; 
                                    var chart = new google.visualization.ColumnChart(document.getElementById('graficoMateria'));
                                    chart.draw(data, options);
                                }");
            strBuilder.Append(@"</script>");
            literal.Text = strBuilder.ToString();
        }
        catch (Exception)
        {

            throw;
        }
    }
}