﻿using BetaPlast.App_Code.Classes;
using BetaPlast.App_Code.Persistencia;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


public partial class Paginas_MateriaPrima_AlterarMateriaPrima : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            Carrega();
        }
    }

    protected void Carrega()
    {
        MateriaPrimaBD bd = new MateriaPrimaBD();
        MateriaPrima materiaprima = bd.Select(Convert.ToInt32(Session["IDmateriaprima"]));
        txtNome.Text = Convert.ToString(materiaprima.Nome);
        txtCor.Text = Convert.ToString(materiaprima.Cor);
        txtFornecedor.Text = Convert.ToString(materiaprima.Fornecedor.Nome);
        txtQuantidadeAtual.Text = Convert.ToString(materiaprima.QuantidadeAtual);
        txtQuantidadeMinima.Text = Convert.ToString(materiaprima.QuantidadeMinima);
        txtPesoEmKG.Text = Convert.ToString(materiaprima.Peso);
    }

    protected void btnEditar_Click(object sender, EventArgs e)
    {
        FornecedorBD fornecedorbd = new FornecedorBD();
        MateriaPrimaBD materiaprimabd = new MateriaPrimaBD();
        MateriaPrima materiaprima = materiaprimabd.Select(Convert.ToInt32(Session["IDmateriaprima"]));

        materiaprima.Nome = txtNome.Text;
        materiaprima.Cor = txtCor.Text;
        materiaprima.Fornecedor = materiaprima.Fornecedor;
        materiaprima.QuantidadeAtual = Convert.ToDouble(txtQuantidadeAtual.Text);
        materiaprima.QuantidadeMinima = Convert.ToDouble(txtQuantidadeMinima.Text);
        materiaprima.Peso = Convert.ToDouble(txtPesoEmKG.Text);

        lblMensagem.Text = materiaprimabd.Update(materiaprima) ? "Estoque de matéria-prima atualizado com sucesso" : "Erro ao atualizar estoque de matéria-prima";
    }
}