﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BetaPlast.App_Code.Classes;
using BetaPlast.App_Code.Persistencia;
using System.Data;

public partial class Paginas_MateriaPrima_ListarMateriaPrima : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        MateriaPrimaBD materiaPrimaBD = new MateriaPrimaBD();
        DataSet ds = materiaPrimaBD.SelectAll();

        int quantidade = ds.Tables[0].Rows.Count;
        if (quantidade > 0)
        {
            grdViewMateriaPrima.DataSource = ds.Tables[0].DefaultView;
            grdViewMateriaPrima.DataBind();
            grdViewMateriaPrima.HeaderRow.TableSection = TableRowSection.TableHeader;

        }
        else
        {
            lblMensagem.Text = "Nenhuma matéria prima cadastrada.";
        }

    }

    protected void grdViewMateriaPrima_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        switch (e.CommandName)
        {
            case "Alterar":
                int codigo = Convert.ToInt32(e.CommandArgument);
                Session["IDmateriaprima"] = codigo;
                Response.Redirect("../MateriaPrima/AlterarMateriaPrima.aspx");
                break;
        }

    }

    
}