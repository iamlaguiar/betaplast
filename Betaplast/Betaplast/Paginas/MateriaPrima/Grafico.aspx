﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Paginas/Master/Index.master" AutoEventWireup="true" CodeFile="Grafico.aspx.cs" Inherits="Paginas_MateriaPrima_Grafico" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MenuLateral" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ConteudoPagina" runat="Server">
    <h3 class="page-header">Estoque de Matéria prima</h3>

    <div id="main" runat="server" style="margin-left: 190px;">

        <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
        <asp:Literal ID="literal" runat="server"></asp:Literal>
        <div id="graficoMateria" style="width: 1000px; height: 300px;">
        </div>
    </div>
</asp:Content>

