﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Paginas/Master/Index.master" AutoEventWireup="true" CodeFile="AlterarMateriaPrima.aspx.cs" Inherits="Paginas_MateriaPrima_AlterarMateriaPrima" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MenuLateral" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ConteudoPagina" runat="Server">
    <h3 class="page-header">Alteraração de matéria prima</h3>

    <div id="main" runat="server" style="margin-left: 200px;" class="container">
        <div class="col-md-9">
            <div class="row">
                <div class="form-group col-md-4">
                    <asp:Label ID="lblNome" runat="server" Text="Nome"></asp:Label>
                    <asp:TextBox ID="txtNome" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
                <div class="form-group col-md-4">
                    <asp:Label ID="lblCor" runat="server" Text="Cor"></asp:Label>
                    <asp:TextBox ID="txtCor" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
                <div class="form-group col-md-4">
                    <asp:Label ID="lblFornecedor" runat="server" Text="Fornecedor"></asp:Label>
                    <asp:TextBox ID="txtFornecedor" runat="server" Enabled="False" CssClass="form-control"></asp:TextBox>
                </div>
            </div>

            <div class="row">
                <div class="form-group col-md-4">
                    <asp:Label ID="lblQuantidadeAtual" runat="server" Text="Quantidade atual de bags"></asp:Label>
                    <asp:TextBox ID="txtQuantidadeAtual" runat="server" CssClass="form-control"></asp:TextBox>
                </div>

                <div class="form-group col-md-4">
                    <asp:Label ID="lblQuantidadeMinima" runat="server" Text="Quantidade mínima de bags"></asp:Label>
                    <asp:TextBox ID="txtQuantidadeMinima" runat="server" CssClass="form-control"></asp:TextBox>
                </div>

                <div class="form-group col-md-4">
                    <asp:Label ID="lblPesoEmKG" runat="server" Text="Peso(KG)"></asp:Label>
                    <asp:TextBox ID="txtPesoEmKG" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
            </div>
            <asp:Button ID="btnEditar" runat="server" Text="Editar" OnClick="btnEditar_Click" CssClass="btn btn-primary" />
            <br />
            <asp:Label ID="lblMensagem" runat="server" Text=""></asp:Label>
        </div>
    </div>
</asp:Content>

