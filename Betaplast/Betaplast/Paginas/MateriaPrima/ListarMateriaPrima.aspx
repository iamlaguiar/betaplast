﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ListarMateriaPrima.aspx.cs" MasterPageFile="~/Paginas/Master/Index.master" Inherits="Paginas_MateriaPrima_ListarMateriaPrima" %>


<asp:Content ID="MenuLateral" ContentPlaceHolderID="MenuLateral" runat="Server">
    <script src="../../Scripts/jquery.dataTables.min.js"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            $('#<%=grdViewMateriaPrima.ClientID%>').dataTable();
        });
    </script>

</asp:Content>

<asp:Content ID="Conteudo" ContentPlaceHolderID="ConteudoPagina" runat="Server">
    <h3 class="page-header">Lista de Matéria prima</h3>

    <div id="main" runat="server" style="margin-left: 200px;" class="container">
        <div class="col-sm-9">
            <asp:Label ID="lblMensagem" runat="server"></asp:Label>
            <br />

            <asp:GridView ID="grdViewMateriaPrima" runat="server" AutoGenerateColumns="False" OnRowCommand="grdViewMateriaPrima_RowCommand"
                AllowPaging="true" CssClass="display" GridLines="None" BorderStyle="none" HorizontalAlign="center">
                <Columns>
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:LinkButton ID="lbAlterar" runat="server" CommandName="Alterar" CommandArgument='<%# Bind("map_codigo")%>'>Alterar</asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:LinkButton ID="lbDeletar" runat="server" CommandName="Desativar" CommandArgument='<%# Bind("map_codigo")%>'>Excluir</asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="map_nome" HeaderText="Nome" />
                    <asp:BoundField DataField="for_nome" HeaderText="Fornecedor" />
                    <asp:BoundField DataField="map_cor" HeaderText="Cor" />
                    <asp:BoundField DataField="map_quantidademinima" HeaderText="Quantidade mínima de bags" />
                    <asp:BoundField DataField="map_quantidadeatual" HeaderText="Quantidade atual de bags" />
                    <asp:BoundField DataField="map_pesoporkg" HeaderText="Peso(KG)" />
                </Columns>
                <HeaderStyle BackColor="#0066ff" ForeColor="White" />

            </asp:GridView>
        </div>
    </div>
</asp:Content>
