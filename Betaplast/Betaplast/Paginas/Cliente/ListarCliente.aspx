﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ListarCliente.aspx.cs" MasterPageFile="~/Paginas/Master/Index.master" Inherits="Paginas_Cliente_ListarCliente" %>


<asp:Content ID="MenuLateral" ContentPlaceHolderID="MenuLateral" runat="Server">
    <script src="../../Scripts/jquery.dataTables.min.js"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            $('#<%=grdViewCliente.ClientID%>').dataTable();
        });
    </script>

</asp:Content>

<asp:Content ID="Conteudo" ContentPlaceHolderID="ConteudoPagina" runat="Server">
    <h3 class="page-header">Lista de clientes</h3>

    <div id="main" runat="server" style="margin-left: 200px;" class="container">
        <div class="col-sm-9">
            <asp:Label ID="lblMensagem" runat="server"></asp:Label>
            <br />
            <asp:GridView ID="grdViewCliente" runat="server" CssClass="display" AutoGenerateColumns="False" OnRowCommand="grdViewCliente_RowCommand" GridLines="None" BorderStyle="None">
                <Columns>
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:LinkButton ID="lbAlterar" runat="server" CommandName="Alterar" CommandArgument='<%# Bind("cli_codigo")%>'>Alterar</asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:LinkButton ID="lbDeletar" runat="server" CommandName="Desativar" CommandArgument='<%# Bind("cli_codigo")%>'>Desativar</asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="cli_nome" HeaderText="Nome" />
                    <asp:BoundField DataField="cli_endereco" HeaderText="Endereço" />
                    <asp:BoundField DataField="cli_telefone" HeaderText="Telefone" />
                    <asp:BoundField DataField="cli_tipo" HeaderText="Tipo" />
                    <asp:BoundField DataField="cli_status" HeaderText="Status" />
                </Columns>
                <HeaderStyle BackColor="#0066ff" ForeColor="White" />
            </asp:GridView>
        </div>
    </div>
</asp:Content>
