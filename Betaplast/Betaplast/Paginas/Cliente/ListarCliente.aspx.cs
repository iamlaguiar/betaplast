﻿using BetaPlast.App_Code.Persistencia;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Paginas_Cliente_ListarCliente : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Carrega();
    }

    protected void Carrega()
    {
        ClienteBD bd = new ClienteBD();
        DataSet ds = bd.SelectAll();
        grdViewCliente.DataSource = ds.Tables[0].DefaultView;
        grdViewCliente.DataBind();
        grdViewCliente.HeaderRow.TableSection = TableRowSection.TableHeader;

        foreach (GridViewRow row in grdViewCliente.Rows)
        {
            if(Convert.ToBoolean(row.Cells[6].Text) == true)
            {
                row.Cells[6].Text = "Ativo";
                row.Cells[6].CssClass = "btn-success";
            }
            else
            {
                row.Cells[6].Text = "Inativo";
                row.Cells[6].CssClass = "btn-danger";
            }
        }
    }

    protected void grdViewCliente_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        {
            int codigo = 0;
            switch (e.CommandName)
            {
                case "Alterar":
                    codigo = Convert.ToInt32(e.CommandArgument);
                    Session["IDCliente"] = codigo;
                    Response.Redirect("AlterarCliente.aspx");
                    break;

                case "Desativar":
                    ClienteBD bd = new ClienteBD();
                    bd.Desativar(Convert.ToInt32(e.CommandArgument));
                    Carrega();
                    break;
            }
        }
    }
}