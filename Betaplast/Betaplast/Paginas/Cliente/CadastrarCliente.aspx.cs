﻿using BetaPlast;
using BetaPlast.App_Code.Classes;
using BetaPlast.App_Code.Persistencia;
using BetaPlast.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Paginas_Cliente_CadastrarCliente : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            Carrega();
        }
    }

    protected void Carrega()
    {
        ddlTipoCliente.Items.Insert(0, "Selecione");

        string[] tipos = System.Enum.GetNames(typeof(EnumCliente));
        foreach (String cliente in tipos)
        {
            int valor = (int)Enum.Parse(typeof(EnumCliente), cliente);
            ListItem item = new ListItem(cliente, valor.ToString());
            ddlTipoCliente.Items.Add(item);
        }

    }

    protected void btnSalvar_Click(object sender, EventArgs e)
    {
        Cliente cliente = new Cliente();

        if (ddlTipoCliente.SelectedItem.Text != "Selecione")
        {

            cliente.Tipo = ddlTipoCliente.SelectedItem.Text;
            cliente.Nome = txtNome.Text;
            cliente.Endereco = txtEndereco.Text;
            cliente.Telefone = txtTelefone.Text;
            cliente.Email = txtEmail.Text;
            if (cliente.Tipo == "Pessoa")
            {
                cliente.CPF = txtTipo.Text;
            }
            else
            {
                cliente.CNPJ = txtTipo.Text;
            }

            ClienteBD bd = new ClienteBD();
            if (!VerificaCliente(cliente))
            {
                if (bd.Insert(cliente))
                {
                    lblMensagem.Text = "Cliente cadastrado com sucesso";
                    LimparCampos();
                }
                else
                {
                    lblMensagem.Text = "Erro ao cadastrar dados";
                }
            }

        }
        else
        {
            lblMensagem.Text = "Selecione o tipo de cliente";
        }
    }

    //

    //

    protected void LimparCampos()
    {
        txtNome.Text = "";
        txtEndereco.Text = "";
        txtTelefone.Text = "";
        txtEmail.Text = "";
        txtTipo.Text = "";
    }

    protected bool VerificaCliente(Cliente cliente)
    {
        bool retorno = false;


        if (EmailEncontrado(cliente.Email))
        {
            lblMensagem.Text = "Email já cadastrado";
            retorno = true;
        }

        if (CpfCnpjEncontrado(cliente))
        {
            if (cliente.Tipo == "Pessoa")
            {
                lblMensagem.Text = "CPF já cadastrado";
                retorno = true;
            }
            else
            {
                lblMensagem.Text = "CNPJ já cadastrado";
                retorno = true;
            }
        }


        return retorno;
    }

    protected bool EmailEncontrado(string email)
    {
        ClienteBD clientebd = new ClienteBD();
        return (clientebd.VerificarEmail(email) != null);
    }

    protected bool CpfCnpjEncontrado(Cliente cliente)
    {
        ClienteBD clientebd = new ClienteBD();
        return (clientebd.VerificarCpfCnpg(cliente) != null);
    }

    protected void LbSair_Click(object sender, EventArgs e)
    {
        Session.Abandon();
        Session.Clear();
        Session.RemoveAll();
        Response.Redirect("../Login.aspx");
    }

}