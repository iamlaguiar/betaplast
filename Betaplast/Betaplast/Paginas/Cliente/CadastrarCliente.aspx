﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="CadastrarCliente.aspx.cs" MasterPageFile="~/Paginas/Master/Index.master" Inherits="Paginas_Cliente_CadastrarCliente" %>

<asp:Content ID="MenuLateral" ContentPlaceHolderID="MenuLateral" runat="Server">

    <script type="text/javascript">
        function updateLabel() {
            var text = ($("#ddlTipoCliente option:selected").text());
            if (text == "Pessoa") {
                document.getElementById("lblTipo").innerText = "CPF";
            }
            else if (text == "Empresa") {
                document.getElementById("lblTipo").innerText = "CNPJ";
            }
        }

    </script>

</asp:Content>
<asp:Content ID="Conteudo1" ContentPlaceHolderID="ConteudoPagina" runat="Server">

    <h3 class="page-header">Cadastro de Cliente</h3>

    <div id="main" runat="server" style="margin-left: 200px;" class="container">
        <div class="col-sm-9">
            <div class="form-group col-md-4">
                <asp:Label ID="lblEscolheTipo" runat="server" Text="Tipo de cliente: "></asp:Label>
                <asp:DropDownList ID="ddlTipoCliente" CssClass="form-control" runat="server" onchange="return updateLabel()"></asp:DropDownList>
            </div>
            <div class="form-group col-md-4">
                <asp:Label ID="lblNome" runat="server" Text="Nome:"></asp:Label>
                <asp:TextBox ID="txtNome" CssClass="form-control" runat="server"></asp:TextBox>
            </div>
            <div class="form-group col-md-4">
                <asp:Label ID="lblEndereco" runat="server" Text="Endereço"></asp:Label>
                <asp:TextBox ID="txtEndereco" CssClass="form-control" runat="server"></asp:TextBox>
            </div>
            <div class="form-group col-md-4">
                <asp:Label ID="lblTelefone" runat="server" Text="Telefone:"></asp:Label>
                <asp:TextBox ID="txtTelefone" CssClass="form-control" runat="server"></asp:TextBox>
            </div>
            <div class="form-group col-md-4">
                <asp:Label ID="lblEmail" runat="server" Text="Email:"></asp:Label>
                <asp:TextBox ID="txtEmail" CssClass="form-control" runat="server"></asp:TextBox>
            </div>
            <div class="form-group col-md-4">
                <asp:Label ID="lblTipo" runat="server" Text=""></asp:Label>
                <asp:TextBox ID="txtTipo" CssClass="form-control" runat="server"></asp:TextBox>
            </div>
            <br />
            <br />
            <hr />
        </div>
        <div class="col-sm-9">
            <div class="form-group col-md-4">
                <asp:Button ID="btnSalvar" CssClass="btn btn-primary" runat="server" Text="Cadastrar" OnClick="btnSalvar_Click" />
                <asp:Button class="btn btn-danger" runat="server" Text="Voltar" ID="btnVoltar" />
                <asp:Label ID="lblMensagem" runat="server" Text=""></asp:Label>
            </div>

        </div>
    </div>
</asp:Content>

