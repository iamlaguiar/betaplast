﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Paginas/Master/Index.master" AutoEventWireup="true" CodeFile="AlterarCliente.aspx.cs" Inherits="Paginas_Cliente_AlterarCliente" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MenuLateral" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ConteudoPagina" runat="Server">
    <h3 class="page-header">Alteraração de Cliente</h3>

    <div id="main" runat="server" style="margin-left: 200px;" class="container">
        <div class="col-sm-9">
            <div class="form-group col-md-4">
                <asp:Label ID="lblEscolheTipo" runat="server" Text="Tipo de cliente: "></asp:Label>
                <asp:TextBox ID="txtTipoSelecionado" runat="server" Enabled="False" CssClass="form-control"></asp:TextBox>
            </div>
            <div class="form-group col-md-4">
                <asp:Label ID="lblNome" runat="server" Text="Nome:"></asp:Label>
                <asp:TextBox ID="txtNome" CssClass="form-control" runat="server"></asp:TextBox>
            </div>
            <div class="form-group col-md-4">
                <asp:Label ID="lblEndereco" runat="server" Text="Endereço"></asp:Label>
                <asp:TextBox ID="txtEndereco" CssClass="form-control" runat="server"></asp:TextBox>
            </div>
            <div class="form-group col-md-4">
                <asp:Label ID="lblTelefone" runat="server" Text="Telefone:"></asp:Label>
                <asp:TextBox ID="txtTelefone" CssClass="form-control" runat="server"></asp:TextBox>
            </div>
            <div class="form-group col-md-4">
                <asp:Label ID="lblEmail" runat="server" Text="Email:"></asp:Label>
                <asp:TextBox ID="txtEmail" CssClass="form-control" runat="server"></asp:TextBox>
            </div>
            <div class="form-group col-md-6">
                <asp:Label ID="lblTipo" runat="server" Text=""></asp:Label>
                <asp:TextBox ID="txtTipo" CssClass="form-control" runat="server"></asp:TextBox>
            </div>
            <br />
            <br />
            <hr />
        </div>
        <div class="col-sm-9">
            <div class="form-group col-md-4">
                <asp:Button ID="btnAlterar" runat="server" Text="Alterar" CssClass="btn btn-primary" OnClick="btnAlterar_Click" />
                <asp:Label ID="lblMensagem" runat="server" Text=""></asp:Label>
            </div>
        </div>
    </div>
</asp:Content>

