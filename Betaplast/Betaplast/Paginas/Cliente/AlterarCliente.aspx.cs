﻿using BetaPlast.App_Code.Classes;
using BetaPlast.App_Code.Persistencia;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Paginas_Cliente_AlterarCliente : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            Carrega();
        }
    }

    private void Carrega()
    {
        ClienteBD clientebd = new ClienteBD();
        Cliente cliente = clientebd.Select(Convert.ToInt32(Session["IDCliente"]));
        txtTipoSelecionado.Text = cliente.Tipo;

        if (cliente.Tipo == "Empresa")
        {
            lblTipo.Text = "CNPJ";
            txtTipo.Text = cliente.CNPJ;
        }
        else if (cliente.Tipo == "Pessoa")
        {
            lblTipo.Text = "CPF";
            txtTipo.Text = cliente.CPF;
        }

        txtNome.Text = cliente.Nome;
        txtEndereco.Text = cliente.Endereco;
        txtTelefone.Text = cliente.Telefone;
        txtEmail.Text = cliente.Email;
    }


    protected void btnAlterar_Click(object sender, EventArgs e)
    {
        ClienteBD clientebd = new ClienteBD();
        Cliente cliente = clientebd.Select(Convert.ToInt32(Session["IDCliente"]));
        cliente.Tipo = txtTipoSelecionado.Text;
        cliente.Nome = txtNome.Text;
        cliente.Endereco = txtEndereco.Text;
        cliente.Telefone = txtTelefone.Text;
        cliente.Email = txtEmail.Text;

        if (cliente.Tipo == "Empresa")
        {
            cliente.CNPJ = txtTipo.Text;
        }
        else if (cliente.Tipo == "Pessoa")
        {
            cliente.CPF = txtTipo.Text;
        }

        if (clientebd.Update(cliente))
        {
            lblMensagem.Text = "Cliente alterado com sucesso";
        } else
        {
            lblMensagem.Text = "Erro ao alterar dados";
        }
    }
}