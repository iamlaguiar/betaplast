﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="RegistrarPedido.aspx.cs" MasterPageFile="~/Paginas/Master/Index.master" Inherits="Paginas_Pedido_RegistrarPedido" %>

<asp:Content ID="MenuLateral" ContentPlaceHolderID="MenuLateral" runat="Server">
</asp:Content>
<asp:Content ID="Conteudo1" ContentPlaceHolderID="ConteudoPagina" runat="Server">
    <h3 class="page-header">Registro de Pedido</h3>

    <div id="main" runat="server" style="margin-left: 190px;" class="container">
        <div class="col-sm-9">
            <div class="form-group col-md-4">
                <asp:Label ID="lblDescricaoPedido" runat="server" Text="Descrição do pedido:"></asp:Label>
                <asp:TextBox ID="txtDescricaoPedido" CssClass="form-control" runat="server"></asp:TextBox>
            </div>
            <div class="form-group col-md-2">
                <asp:Label ID="lblQuantidadePedido" runat="server" Text="Quantidade:"></asp:Label>
                <asp:TextBox ID="txtQuantidadePedido" CssClass="form-control" runat="server"></asp:TextBox>
            </div>
            <div class="form-group col-md-3">
                <asp:Label ID="lblProduto" runat="server" Text="Selecione o produto:"></asp:Label>
                <asp:DropDownList ID="ddlProduto" CssClass="form-control" runat="server"></asp:DropDownList>
            </div>
            <div class="form-group col-md-3">
                <asp:Label ID="lblCliente" runat="server" Text="Selecione o cliente:"></asp:Label>
                <asp:DropDownList ID="ddlCliente" CssClass="form-control" runat="server"></asp:DropDownList>
            </div>
            <div class="form-group col-md-4">
                <asp:Label ID="lblDataEntrega" runat="server" Text="Data de entrega:"></asp:Label>
                <asp:TextBox ID="txtDataEntrega" class="form-control date" runat="server"></asp:TextBox>
            </div>
            <div class="form-group col-md-8">
                <asp:Label ID="lblObservacoes" runat="server" Text="Observações:"></asp:Label>
                <asp:TextBox ID="txtObservacoes" CssClass="form-control" runat="server"></asp:TextBox>
            </div>
            <br />
            <br />
        </div>
        <div class="form-group col-md-4" style="margin-left: 10px;">
            <asp:Button ID="btnPedir" class="btn btn-primary" runat="server" Text="Realizar pedido" OnClick="btnPedir_Click" />
            <asp:Button class="btn btn-danger" runat="server" Text="Voltar" ID="btnVoltar" />
            <asp:Label ID="lblMensagem" runat="server" Text=""></asp:Label>
        </div>
    </div>
</asp:Content>
