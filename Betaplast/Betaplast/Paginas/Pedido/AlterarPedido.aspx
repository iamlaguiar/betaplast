﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="AlterarPedido.aspx.cs" MasterPageFile="~/Paginas/Master/Index.master" Inherits="Paginas_Pedido_AlterarPedido" %>

<asp:Content ID="MenuLateral" ContentPlaceHolderID="MenuLateral" runat="Server">
</asp:Content>
<asp:Content ID="Conteudo1" ContentPlaceHolderID="ConteudoPagina" runat="Server">

    <h3 class="page-header">Alteração de Pedido</h3>

    <div id="main" runat="server" style="margin-left: 190px;" class="container">
        <div class="col-sm-9">
            <div class="form-group col-md-4">
                <asp:Label ID="lblDescricaoPedido" runat="server" Text="Descrição do pedido:"></asp:Label>
                <asp:TextBox ID="txtDescricaoPedido" CssClass="form-control" runat="server"></asp:TextBox>
            </div>
            <div class="form-group col-md-2">
                <asp:Label ID="lblQuantidadePedido" runat="server" Text="Quantidade:"></asp:Label>
                <asp:TextBox ID="txtQuantidadePedido" CssClass="form-control" runat="server"></asp:TextBox>
            </div>
            <div class="form-group col-md-3">
                <asp:Label ID="lblProduto" runat="server" Text="Selecione o produto:"></asp:Label>
                <asp:DropDownList ID="ddlProduto" CssClass="form-control" runat="server"></asp:DropDownList>
            </div>
            <div class="form-group col-md-3">
                <asp:Label ID="lblCliente" runat="server" Text="Selecione o cliente:"></asp:Label>
                <asp:DropDownList ID="ddlCliente" CssClass="form-control" runat="server"></asp:DropDownList>
            </div>
            <div class="form-group col-md-4">
                <asp:Label ID="lblDataEntrega" runat="server" Text="Data de entrega:"></asp:Label>
                <asp:TextBox ID="txtDataEntrega" class="form-control date" runat="server"></asp:TextBox>
            </div>
            <div class="form-group col-md-4">
                <asp:Label ID="lblDataPedido" runat="server" Text="Data de realização do pedido:"></asp:Label>
                <asp:TextBox ID="txtDataPedido" class="form-control date" runat="server"></asp:TextBox>
            </div>

            <div class="form-group col-md-6">
                <asp:Label ID="lblViscosidade" runat="server" Text="Viscosidade"></asp:Label>
                <br />
                <asp:TextBox ID="txtViscosidade" runat="server" CssClass="form-control"></asp:TextBox>&nbsp   
    <asp:LinkButton ID="lkBtnCompararViscosidade" runat="server" OnClick="lkBtnCompararViscosidade_Click"> Comparar viscosidade</asp:LinkButton>
                <br />
                <asp:Label ID="lblMensagemViscosidade" runat="server" Text=""></asp:Label>
                <br />
                <br />

                <asp:Label ID="lblRaio" runat="server" Text="Raio"></asp:Label>
                <br />
                <asp:TextBox ID="txtRaio" runat="server" CssClass="form-control"></asp:TextBox>&nbsp 
     <asp:LinkButton ID="lkBtnCompararRaio" runat="server" OnClick="lkBtnCompararRaio_Click">Comparar raio</asp:LinkButton>
                <br />
                <asp:Label ID="lblMensagemRaio" runat="server" Text=""></asp:Label>
                <br />
                <br />
            </div>

            <div class="form-group col-md-12">
                <div class="form-group col-md-8">
                    <asp:Label ID="lblObservacoes" runat="server" Text="Observações:"></asp:Label>
                    <asp:TextBox ID="txtObservacoes" CssClass="form-control" runat="server"></asp:TextBox>
                </div>
                <div class="form-group col-md-4">
                    <asp:Label ID="lblStatus" runat="server" Text="Status do pedido:"></asp:Label>
                    <asp:DropDownList ID="ddlStatus" runat="server" CssClass="form-control"></asp:DropDownList>
                </div>
            </div>

            <br />
            <br />
        </div>
        <div class="form-group col-md-4" style="margin-left: 10px;">
            <asp:Button ID="btnAtualizar" class="btn btn-primary" runat="server" Text="Alterar dados" OnClick="btnFinalizar_Click" />
            <%--<asp:Button class="btn btn-danger" runat="server" Text="Voltar" ID="btnVoltar" />--%>
            <asp:Label ID="lblMensagem" runat="server" Text=""></asp:Label>
        </div>
    </div>
</asp:Content>
