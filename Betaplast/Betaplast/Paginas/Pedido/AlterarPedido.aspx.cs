﻿using BetaPlast.App_Code.Classes;
using BetaPlast.App_Code.Persistencia;
using BetaPlast.Util;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Paginas_Pedido_AlterarPedido : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            Carrega();
        }
    }

    protected void Carrega()
    {
        PedidosBD pedidobd = new PedidosBD();
        Pedido pedido = pedidobd.Select(Convert.ToInt32(Session["IDpedido"]));

        txtDescricaoPedido.Text = Convert.ToString(pedido.Descricao);
        txtQuantidadePedido.Text = Convert.ToString(pedido.Quantidade);
        txtDataEntrega.Text = pedido.DataEntrega.ToString("yyyy-MM-dd");
        txtDataPedido.Text = pedido.DataPedido.ToString("yyyy-MM-dd");
        txtObservacoes.Text = Convert.ToString(pedido.Obersevacoes);


        ProdutoBD produtobd = new ProdutoBD();
        DataSet produtods = produtobd.SelectAll();

        ddlProduto.DataSource = produtods.Tables[0].DefaultView;
        ddlProduto.DataTextField = "pro_descricao";
        ddlProduto.DataValueField = "pro_codigo";
        ddlProduto.DataBind();
        ddlProduto.SelectedItem.Text = pedido.Produto.DescricaoProduto;

        ClienteBD clientebd = new ClienteBD();
        DataSet clienteds = clientebd.SelectAll();

        ddlCliente.DataSource = clienteds.Tables[0].DefaultView;
        ddlCliente.DataTextField = "cli_nome";
        ddlCliente.DataValueField = "cli_codigo";
        ddlCliente.DataBind();
        ddlCliente.SelectedItem.Text = pedido.Cliente.Nome;



        string[] ListaStatus = System.Enum.GetNames(typeof(EnumStatus));
        //foreach (String status in ListaStatus)
        //{
        //    int valor = (int)Enum.Parse(typeof(EnumStatus), status);
        //    ListItem item = new ListItem(status, valor.ToString());
        //    ddlStatus.Items.Add(item);
        //}


        for (int i = 0; i < ListaStatus.Length; i++)
        {
            int valor = (int)Enum.Parse(typeof(EnumStatus), ListaStatus[i]);
            ListItem item = new ListItem(ListaStatus[i ], (valor).ToString());
            ddlStatus.Items.Add(item);
        }
        ddlStatus.DataBind();
        ddlStatus.SelectedItem.Text = pedido.Status;

    }

    protected void lkBtnCompararViscosidade_Click(object sender, EventArgs e)
    {
        PedidosBD pedidobd = new PedidosBD();
        Pedido pedido = pedidobd.Select(Convert.ToInt32(Session["IDpedido"]));

        double viscosidade = Convert.ToDouble(txtViscosidade.Text);

        if (pedido.Produto.Viscosidade != viscosidade)
        {
            lblMensagemViscosidade.Text = "A viscosidade ideal é " + pedido.Produto.Viscosidade.ToString();
        }
        else
        {
            lblMensagemViscosidade.Text = "A viscosidade está correta";
        }
    }

    protected void lkBtnCompararRaio_Click(object sender, EventArgs e)
    {
        PedidosBD pedidobd = new PedidosBD();
        Pedido pedido = pedidobd.Select(Convert.ToInt32(Session["IDpedido"]));

        double raio = Convert.ToDouble(txtRaio.Text);

        if (pedido.Produto.Raio != raio)
        {
            lblMensagemRaio.Text = "O raio ideal é " + pedido.Produto.Raio.ToString();
        }
        else
        {
            lblMensagemRaio.Text = "O raio está correta";
        }
    }

    protected void btnFinalizar_Click(object sender, EventArgs e)
    {
        PedidosBD pedidobd = new PedidosBD();
        ClienteBD clientebd = new ClienteBD();
        ProdutoBD produtobd = new ProdutoBD();

        Pedido pedido = pedidobd.Select(Convert.ToInt32(Session["IDpedido"]));

        pedido.Descricao = txtDescricaoPedido.Text;
        pedido.Produto = produtobd.Select(Convert.ToInt32(ddlProduto.SelectedItem.Value));
        pedido.Cliente = clientebd.Select(Convert.ToInt32(ddlCliente.SelectedItem.Value));
        pedido.Quantidade = Convert.ToInt32(txtQuantidadePedido.Text);
        pedido.DataEntrega = Convert.ToDateTime(txtDataEntrega.Text);
        pedido.DataPedido = Convert.ToDateTime(txtDataPedido.Text);
        pedido.Status = ddlStatus.SelectedItem.Text;

        if (pedidobd.UpdatePedido(pedido))
        {
            lblMensagem.Text = "Dados do pedido atualizados com sucesso";
        }
        else
        {
            lblMensagem.Text = "Erro ao atualiar dados";
        }
    }
}