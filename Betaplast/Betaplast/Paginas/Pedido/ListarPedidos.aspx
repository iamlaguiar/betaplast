﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ListarPedidos.aspx.cs" MasterPageFile="~/Paginas/Master/Index.master" Inherits="Paginas_Pedido_ListarPedidos" %>


<asp:Content ID="MenuLateral" ContentPlaceHolderID="MenuLateral" runat="Server">
    <script type="text/javascript">
        $(document).ready(function () {
            $('#<%=grdViewPedidos.ClientID%>').dataTable();
        });

        $(document).ready(function () {
            $('#<%=grdViewNaoRecebidos.ClientID%>').dataTable();
        });
    </script>

</asp:Content>

<asp:Content ID="Conteudo" ContentPlaceHolderID="ConteudoPagina" runat="Server">
    <h3 class="page-header">Lista de pedidos</h3>

    <div id="main" runat="server" style="margin-left: 190px;" class="container">
        <div class="col-sm-9">
            <asp:Label ID="lblMensagem" runat="server"></asp:Label>
            <br />
            <asp:GridView ID="grdViewNaoRecebidos" runat="server" AutoGenerateColumns="False" OnRowCommand="grdViewNaoRecebidos_RowCommand"
                AllowPaging="true" CssClass="display" BorderStyle="None" BorderWidth="1px" CellPadding="3" GridLines="None" HorizontalAlign="Center" RowStyle-CssClass="Rows">
                <Columns>
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:LinkButton ID="lblConfirmarRecebimento" runat="server" CommandName="Confirmar" CommandArgument='<%# Bind("ped_codigo")%>'>Confirmar recebimento</asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="ped_descricao" HeaderText="Descrição" />
                    <asp:BoundField DataField="ped_quantidade" HeaderText="Quantidade" />
                    <asp:BoundField DataField="ped_dataentrega" HeaderText="Data de entrega" DataFormatString="{0:dd/MM/yyyy}" />
                    <asp:BoundField DataField="pro_descricao" HeaderText="Produto" />

                </Columns>
                <HeaderStyle BackColor="#0066ff" ForeColor="White" HorizontalAlign="Center" />

            </asp:GridView>
            <asp:Label ID="lblMensagemNaoRecebido" Text="" runat="server"></asp:Label>


            <br />
            <br />
            <br />
            <asp:GridView ID="grdViewPedidos" runat="server" AutoGenerateColumns="False" OnRowCommand="grdViewPedidos_RowCommand"
                AllowPaging="true" CssClass="display" BorderStyle="None" BorderWidth="1px" CellPadding="3" GridLines="None" HorizontalAlign="Center" RowStyle-CssClass="Rows">
                <Columns>
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:LinkButton ID="lblVerDetalhes" runat="server" CommandName="VerDetalhes" CommandArgument='<%# Bind("ped_codigo")%>'>Detalhes</asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:LinkButton ID="lblCancelar" runat="server" CommandName="Cancelar" CommandArgument='<%# Bind("ped_codigo")%>'>Cancelar pedido</asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:BoundField DataField="ped_descricao" HeaderText="Descrição" />
                    <asp:BoundField DataField="ped_quantidade" HeaderText="Quantidade" />
                    <asp:BoundField DataField="ped_dataentrega" HeaderText="Data de entrega" DataFormatString="{0:dd/MM/yyyy}" />
                    <asp:BoundField DataField="pro_descricao" HeaderText="Produto" />
                    <asp:BoundField DataField="ped_status" HeaderText="Status" />
                </Columns>
                <HeaderStyle BackColor="#0066ff" ForeColor="White" HorizontalAlign="Center" />

            </asp:GridView>
        </div>
        <asp:Label ID="lblMensagemRecebido" Text="" runat="server"></asp:Label>
    </div>
</asp:Content>
