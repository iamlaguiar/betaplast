﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BetaPlast.App_Code.Classes;
using BetaPlast.App_Code.Persistencia;
using System.Data;

public partial class Paginas_Pedido_RegistrarPedido : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            Carrega();
        }
    }

    private void Carrega()
    {
        ProdutoBD produtobd = new ProdutoBD();
        DataSet produtods = produtobd.SelectAll();

        ddlProduto.DataSource = produtods.Tables[0].DefaultView;
        ddlProduto.DataTextField = "pro_descricao";
        ddlProduto.DataValueField = "pro_codigo";
        ddlProduto.DataBind();
        ddlProduto.Items.Insert(0, "Selecione");

        ClienteBD clientebd = new ClienteBD();
        DataSet clienteds = clientebd.SelectAll();

        ddlCliente.DataSource = clienteds.Tables[0].DefaultView;
        ddlCliente.DataTextField = "cli_nome";
        ddlCliente.DataValueField = "cli_codigo";
        ddlCliente.DataBind();
        ddlCliente.Items.Insert(0, "Selecione");
    }

    protected void btnPedir_Click(object sender, EventArgs e)
    {
        if (!HasCliente())
        {
            lblMensagem.Text = "Selecione um cliente";
        }

        if (!HasProduto())
        {
            lblMensagem.Text = "Selecione um produto";
        }

        ProdutoBD produtobd = new ProdutoBD();
        ClienteBD clientebd = new ClienteBD();
        

        Pedido pedido = new Pedido();

        pedido.Descricao = txtDescricaoPedido.Text;
        pedido.Quantidade = Convert.ToInt32(txtQuantidadePedido.Text);
        pedido.DataEntrega = Convert.ToDateTime(txtDataEntrega.Text);
        pedido.Obersevacoes = txtObservacoes.Text;
        pedido.Status = "Registrado";
        pedido.Produto = produtobd.Select(Convert.ToInt32(ddlProduto.SelectedItem.Value));
        pedido.Cliente = clientebd.Select(Convert.ToInt32(ddlCliente.SelectedItem.Value));

        PedidosBD pedidosbd = new PedidosBD();
        if (pedidosbd.InsertPedido(pedido))
        {
            lblMensagem.Text = "Pedido registrado com sucesso";
            LimparCampos();
        }
        else
        {
            lblMensagem.Text = "Erro ao registrar pedido";
        }
    }

    protected void LimparCampos()
    {
        txtDescricaoPedido.Text = "";
        txtQuantidadePedido.Text = "";
        txtDataEntrega.Text = "";
        txtObservacoes.Text = "";

        ddlCliente.SelectedItem.Text = "Selecione";
        ddlProduto.SelectedItem.Text = "Selecione";
    }

    private bool HasCliente()
    {
        return (ddlCliente.SelectedItem.Text != "Selecione");
    } 

    private bool HasProduto()
    {
        return (ddlProduto.SelectedItem.Text != "Selecione");
    }

}