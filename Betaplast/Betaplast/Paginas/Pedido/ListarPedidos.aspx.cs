﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using BetaPlast.App_Code.Classes;
using BetaPlast.App_Code.Persistencia;

public partial class Paginas_Pedido_ListarPedidos : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Carrega();
    }

    protected void Carrega()
    {

        PedidosBD pedidosbd = new PedidosBD();
        DataSet pedidosrecebidosds = pedidosbd.SelectAllWithProduto();
        DataSet pedidosnaorecebidosds = pedidosbd.SelectNaoRecebidos();

        int quantidadenaorecebidos = pedidosnaorecebidosds.Tables[0].Rows.Count;
        if (quantidadenaorecebidos > 0)
        {
            grdViewNaoRecebidos.DataSource = pedidosnaorecebidosds.Tables[0].DefaultView;
            grdViewNaoRecebidos.DataBind();
            grdViewNaoRecebidos.HeaderRow.TableSection = TableRowSection.TableHeader;
        }
        else
        {
            grdViewNaoRecebidos.Visible = false;
            lblMensagemNaoRecebido.Text = "Não existe novo pedido";
        }


        int quantidaderecebidos = pedidosrecebidosds.Tables[0].Rows.Count;
        if (quantidaderecebidos > 0)
        {
            grdViewPedidos.DataSource = pedidosrecebidosds.Tables[0].DefaultView;
            grdViewPedidos.DataBind();
            grdViewPedidos.HeaderRow.TableSection = TableRowSection.TableHeader;

            foreach (GridViewRow rows in grdViewPedidos.Rows)
            {
                string status = Convert.ToString(rows.Cells[6].Text);
                switch (status)
                {
                    case "Finalizado":
                        rows.Cells[6].CssClass = "btn-success";
                        //rows.Cells[1].Enabled = false;
                        break;

                    case "Extrusora":
                        rows.Cells[6].BackColor = System.Drawing.ColorTranslator.FromHtml("#0033cc");
                        rows.Cells[6].ForeColor = System.Drawing.Color.White;
                        break;
                    case "Impressão":
                        rows.Cells[6].BackColor = System.Drawing.ColorTranslator.FromHtml("#0033cc");
                        rows.Cells[6].ForeColor = System.Drawing.Color.White;
                        break;
                    case "Corte":
                        rows.Cells[6].BackColor = System.Drawing.ColorTranslator.FromHtml("#0033cc");
                        rows.Cells[6].ForeColor = System.Drawing.Color.White;
                        break;
                    case "Estoque":
                        rows.Cells[6].BackColor = System.Drawing.ColorTranslator.FromHtml("#0033cc");
                        rows.Cells[6].ForeColor = System.Drawing.Color.White;
                        break;

                    case "Enviando":
                        rows.Cells[6].BackColor = System.Drawing.ColorTranslator.FromHtml("#006600");
                        rows.Cells[6].ForeColor = System.Drawing.Color.White;
                        break;

                    case "Entregue":
                        rows.Cells[6].CssClass = "btn-success";
                        break;

                    case "Reprovado":
                        rows.Cells[6].CssClass = "btn-danger";
                        break;

                        /*
                   Extrusora = 1,  
                   Impressão = 2,
                   Corte = 3,
                   Estoque = 4,
                   Enviando = 5,
                   Finalizado = 6,
                   Reprovado = 7,
                   Entregue = 8
                        */
                }
            }
        }
        else
        {
            lblMensagemRecebido.Text = "Não existe nenhum pedido cadastrado";
        }
    }

    protected void grdViewPedidos_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        int codigo = 0;
        PedidosBD pedidobd = new PedidosBD();

        switch (e.CommandName)
        {
            case "VerDetalhes":
                codigo = Convert.ToInt32(e.CommandArgument);
                Session["IDpedido"] = codigo;
                Response.Redirect("AlterarPedido.aspx");
                break;

            case "Cancelar":
                pedidobd.CancelarPedido(Convert.ToInt32(e.CommandArgument));
                Carrega();
                break;
                

        }
    }

    protected void grdViewNaoRecebidos_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        switch (e.CommandName)
        {
            case "Confirmar":
                PedidosBD pedidobd = new PedidosBD();
                pedidobd.ConfirmarRecebimento(Convert.ToInt32(e.CommandArgument));
                Carrega();
                break;
        }
    }
}