﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Login.aspx.cs" Inherits="Paginas_Login" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>

    <link href="../Content/css/bootstrap.css" rel="stylesheet" />

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">

    <script src="../Scripts/bootstrap.min.js"></script>

    <script src="../Scripts/jquery-3.2.1.min.js"></script>



</head>
<body style="background-image: url('../content/img/7.jpg')">
    <div class="container">

        <div class="modal-content" style="max-width: 330px; height: 320px; background: rgba(35, 35, 35, 0.65); color: #fff; margin: 0 auto; margin-top: 170px;">
            <div class="form-top" style="padding: 1px;">
                <div class="modal-header">
                    <h2>Bem Vindo</h2>
                    <p>Digite seu e-mail e senha para acessar:</p>
                    <asp:Label ID="lblMensagem" Style="color: #ff8d8d;" runat="server"></asp:Label>
                </div>
            </div>
            <div class="modal-body">
                <form class="form-signin" id="form1" runat="server">
                    <div class="input-group">
                        <span class="input-group-addon" id="basic-addon1"><b>@</b></span>
                        <asp:TextBox ID="txtEmail" runat="server" class="form-control" placeholder="E-mail"></asp:TextBox>
                    </div>
                    <br />
                    <div class="input-group">
                        <span class="input-group-addon" id="basic-addon1"><span class="glyphicon glyphicon-lock"></span></span>

                        <asp:TextBox ID="txtSenha" TextMode="Password" runat="server" class="form-control" placeholder="Senha"></asp:TextBox>

                    </div>

                    <br />

                    <div class="input-group col-xs-12 col-sm-12 col-lg-12">

                        <asp:Button ID="btnEntrar" class="btn btn-primary col-md-12" runat="server" Text="Entrar" OnClick="BtnEntrar_Click" />

                        <br />
                        <br />

                    </div>

                </form>

            </div>

        </div>

    </div>


</body>
</html>
