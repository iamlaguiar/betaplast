﻿using BetaPlast.App_Code.Classes;
using BetaPlast.App_Code.Persistencia;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Paginas_Index : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        //GrantAccess();
    }

    protected void GrantAccess()
    {
        MembroBD membrobd = new MembroBD();
        Membro membro = membrobd.Select(Convert.ToInt32(Session["IDusuario"]));
        if(membro.Tipo == 0)
        {
            ContentPlaceHolder placeHolder = (ContentPlaceHolder)Master.FindControl("MenuLateral");
            HyperLink hpl = (HyperLink)placeHolder.FindControl("hplAdicionarProduto");
            hpl.Visible = false;
        }
    }
}