﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Index.aspx.cs" MasterPageFile="~/Paginas/Master/Index.master" Inherits="Paginas_Administrador_Index" %>

<asp:content id="MenuLateral" contentplaceholderid="MenuLateral" runat="Server">
       
</asp:content>
<asp:content id="Conteudo" contentplaceholderid="ConteudoPagina" runat="Server">
    <div id="main" runat="server" style="margin-left: 200px;" class="container">
        <div class="col-sm-9">
            <!-- column 2 -->
            <h3 class="page-header">
                <asp:Label ID="lblTitulo" runat="server" class="page-header" Text="Tela Principal do Administrador"></asp:Label>
            </h3>
            <div class="row">
                <!--center-right-->
                <div class="col-sm-12">
                    <p>
                        Este é o Painel de Gestão Integrado da Betaplast.
                    </p>
                    <hr>
                    <div class="col-sm-12">
                    <div class="btn-group btn-group-justified" role="group">
                          <Button class="btn btn-info ">
                            <i class="glyphicon glyphicon-plus"></i><br>
                            Produtos
                           </Button>
                        <Button class="btn btn-info ">
                            <i class="glyphicon glyphicon-cloud"></i>
                            <br>
                            Pedidos
                        </Button>
                        <button class="btn btn-info">
                            <i class="glyphicon glyphicon-cog"></i>
                            <br>
                            Fornecedores
                        </button>
                        <button class="btn btn-info ">
                            <i class="glyphicon glyphicon-question-sign"></i>
                            <br>
                            Clientes
                        </button>
                        <button class="btn btn-info ">
                            <i class="glyphicon glyphicon-question-sign"></i>
                            <br>
                            Relatorios
                        </button>
                    </div>

                </div>
                <!--/col-span-6-->
                </div>
                
            </div>
            <!--/row-->
        </div>

    </div>
</asp:content>