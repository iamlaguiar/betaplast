﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BetaPlast.App_Code.Classes;
using BetaPlast.App_Code.Persistencia;

public partial class Paginas_Login : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void BtnEntrar_Click(object sender, EventArgs e)
    {
        string email = txtEmail.Text.Trim();
        string senha = txtSenha.Text.Trim();

        if (!IsPreenchido(email))
        {
            lblMensagem.Text = "Preencha o email";
            txtEmail.Focus();
            return;
        }
        if (!IsPreenchido(senha))
        {
            lblMensagem.Text = "Preencha a senha";
            txtSenha.Focus();
            return;
        }
        MembroBD bd = new MembroBD();
        Membro Membro = new Membro();
        Membro = bd.Autentica(email, senha);
        if (!UsuarioEncontrado(Membro))
        {
            lblMensagem.Text = "Usuário não encontrado";
            txtEmail.Focus();
            return;
        }
        Session["IDusuario"] = Membro.Codigo;
        switch (Membro.Tipo)
        {
            case 0:
                Response.Redirect("Index/Index.aspx");
                break;
            case 1:
                Response.Redirect("Gerente/Index.aspx");
                break;
            case 2:
                Response.Redirect("Funcionario/Index.aspx");
                break;
            case 3:
                Response.Redirect("Estoquista/Index.aspx");
                break;

            default:
                break;
        }


    }


    private bool IsPreenchido(string str)
    {
        bool retorno = false;
        if (str != string.Empty)
        {
            retorno = true;
        }
        return retorno;
    }
    private bool UsuarioEncontrado(Membro membro)
    {
        bool retorno = false;
        if (membro != null)
        {
            retorno = true;
        }
        return retorno;
    }
}