﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BetaPlast.App_Code.Classes;
using BetaPlast.App_Code.Persistencia;
using System.Data;

public partial class Paginas_Master_Index : System.Web.UI.MasterPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Access();

    }

    protected void LbSair_Click(object sender, EventArgs e)
    {
        Session.Abandon();
        Session.Clear();
        Session.RemoveAll();
        Response.Redirect("../Login.aspx");
    }

    public void Access()
    {
        MembroBD membrobd = new MembroBD();
        Membro membro = membrobd.Select(Convert.ToInt32(Session["IDusuario"]));
        lblUsuario.Text = membro.Nome;
        if (membro.Tipo == 0)
        {
            hplAdicionarProduto.Visible = true;
            hplGraficoProduto.Visible = true;
            hplListarProdutos.Visible = true;
            hplCadastrarCliente.Visible = true;
            hplListarCliente.Visible = true;
            hplCadastrarFornecedor.Visible = true;
            hplListarFornecedor.Visible = true;
            hplCadastrarFuncionario.Visible = true;
            hplRegistrarBag.Visible = true;
            hplGraficoMateria.Visible = true;
            hplListarMateria.Visible = true;
            hplRegistrarPedidos.Visible = true;
            hplListarPedidos.Visible = true;
            hplPerdas.Visible = true;
            hplGraficoPerdas.Visible = true;
        }
        else if (membro.Tipo == 1)
        {

        }
        else if (membro.Tipo == 2)
        {

        }
        else if(membro.Tipo == 3)
        {

        }
    }
}

