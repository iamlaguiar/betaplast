﻿using BetaPlast.App_Code.Classes;
using BetaPlast.App_Code.Persistencia;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Paginas_Estoquista_Index : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        int codigo = Convert.ToInt32(Session["IDusuario"]);
        MembroBD bd = new MembroBD();
        Membro membro = bd.Select(codigo);
        if (!IsEstoquista(membro.Tipo))
        {
            Response.Redirect("../Erro/AcessoNegado.aspx");
        }
        else
        {
            lblTitulo.Text = "Bem vindo (Estoquista) : " + membro.Nome;
        }
    }
    protected void LbSair_Click(object sender, EventArgs e)
    {
        Session.Abandon();
        Session.Clear();
        Session.RemoveAll();
        Response.Redirect("../Login.aspx");
    }

    private bool IsEstoquista(int tipo)
    {
        bool retorno = false;
        if (tipo == 3)
        {
            retorno = true;
        }
        return retorno;
    }
}