﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Index.aspx.cs" MasterPageFile="~/Paginas/Master/Index.master" Inherits="Paginas_Estoquista_Index" %>

<asp:content id="MenuLateral" contentplaceholderid="MenuLateral" runat="Server">
    <li class="dropdown"><a class="dropdown-toggle" href="#" data-toggle="dropdown"><i class="glyphicon glyphicon-flash"></i> Produtos </a>
        <ul class="dropdown-menu dropdown-menu-left" style="list-style-type:none;">
            <li><asp:HyperLink ID="hplListarProdutos" runat="server" NavigateUrl="~/Paginas/Produto/ListarProduto.aspx">Listar Produto</asp:HyperLink></li>
        </ul>
     </li>
     <li class="dropdown"><a class="dropdown-toggle" href="#" data-toggle="dropdown"><i class="glyphicon glyphicon-cloud"></i> Pedidos</a>
        <ul class="dropdown-menu dropdown-menu-left" style="list-style-type:none;">
            <li><asp:HyperLink ID="hplListarPedidos" runat="server" NavigateUrl="~/Paginas/Pedidos/ListarPedidos.aspx">Listar Pedidos</asp:HyperLink></li>
        </ul>
     </li>
     <li class="dropdown"><a <a class="dropdown-toggle" data-toggle="dropdown" href="#" target="ext"><i class="glyphicon glyphicon-briefcase"></i> Fornecedores</a>
        <ul class="dropdown-menu dropdown-menu-left" style="list-style-type:none;">
            <li><asp:HyperLink ID="hplListarFornecedor" runat="server" NavigateUrl="~/Paginas/Fornecedor/ListarFornecedor.aspx">Listar Fornecedores</asp:HyperLink></li>
        </ul>
    </li>
    <li class="dropdown"><a <a class="dropdown-toggle" data-toggle="dropdown" href="#" target="ext"><i class="fa fa-users"></i> Clientes</a>
        <ul class="dropdown-menu dropdown-menu-left" style="list-style-type:none;">
            <li><asp:HyperLink ID="hplCadastrarCliente" runat="server" NavigateUrl="~/Paginas/Cliente/CadastrarCliente.aspx">Cadastro de Clientes</asp:HyperLink></li>
            <li><asp:HyperLink ID="hplListarCliente" runat="server" NavigateUrl="~/Paginas/Cliente/ListarCliente.aspx">Listar Clientes</asp:HyperLink></li>
        </ul>
    </li>   
    <li><a href="#" class="dropdown-toggle" data-toggle="dropdown" target="ext"><i class="glyphicon glyphicon-list-alt"></i> Funcionários</a>
        <ul class="dropdown-menu dropdown-menu-left" style="list-style-type:none;">
            <li><asp:HyperLink ID="hplCadastrarFuncionario" runat="server" NavigateUrl="~/Paginas/Funcionario/CadastrarFuncionario.aspx">Cadastro de Funcionário</asp:HyperLink></li>
            <li><asp:HyperLink ID="hplListarFuncionario" runat="server" NavigateUrl="~/Paginas/Funcionarios/ListarFuncionario.aspx">Listar Funcionarios</asp:HyperLink></li>
        </ul>
     </li>
        <li><a href="#" class="dropdown-toggle" data-toggle="dropdown" target="ext"><i class="glyphicon glyphicon-oil"></i>Matéria-prima</a>
                        <ul class="dropdown-menu dropdown-menu-left" style="list-style-type: none;">
                            <li>
                                <asp:HyperLink ID="hplListarMateriaPrima" runat="server" NavigateUrl="~/Paginas/MateriaPrima/ListarMateriaPrima.aspx">Listar matéria-prima</asp:HyperLink></li>
                        </ul>
                    </li>
            <li><a href="http://www.bootply.com/85861" target="ext"><i class="fa fa-user"></i> Conta</a></li>
       
</asp:content>
<asp:content id="Conteudo" contentplaceholderid="ConteudoPagina" runat="Server">
    <div id="main" runat="server" style="margin-left: 200px;" class="container">
        <div class="col-sm-9">
            <!-- column 2 -->
            <h3 class="page-header">
                <asp:Label ID="lblTitulo" runat="server" Text="Tela Principal do Estoquista"></asp:Label>
            </h3>
             <div class="row">
                <!--center-right-->
                <div class="col-sm-12">
                    <p>
                        Este é o Painel de Gestão Integrado da Betaplast.
                    </p>
                    <hr>
                    <div class="col-sm-12">
                    <div class="btn-group btn-group-justified" role="group">
                          <Button class="btn btn-info ">
                            <i class="glyphicon glyphicon-plus"></i><br>
                            Produtos
                           </Button>
                        <Button class="btn btn-info ">
                            <i class="glyphicon glyphicon-cloud"></i>
                            <br>
                            Pedidos
                        </Button>
                        <button class="btn btn-info">
                            <i class="glyphicon glyphicon-cog"></i>
                            <br>
                            Fornecedores
                        </button>
                        <button class="btn btn-info ">
                            <i class="glyphicon glyphicon-question-sign"></i>
                            <br>
                            Clientes
                        </button>
                        <button class="btn btn-info ">
                            <i class="glyphicon glyphicon-question-sign"></i>
                            <br>
                            Relatorios
                        </button>
                    </div>

                </div>
                <!--/col-span-6-->
                </div>
                
            </div>
            <!--/row-->
        </div>

    </div>
</asp:content>
        