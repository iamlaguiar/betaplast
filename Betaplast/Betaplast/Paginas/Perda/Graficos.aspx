﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Paginas/Master/Index.master" AutoEventWireup="true" CodeFile="Graficos.aspx.cs" Inherits="Paginas_Perda_Graficos" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MenuLateral" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ConteudoPagina" runat="Server">
    <h3 class="page-header">Gráficos de perdas</h3>

    <div id="main" runat="server" style="margin-left: 200px;" class="container">

        <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

        <asp:Literal ID="literalMp" runat="server"></asp:Literal>
        <asp:Literal ID="literal" runat="server"></asp:Literal>
        <div id="graficoMp" style="width: 900px; height: 300px;">
        </div>
        <br />
        <div id="graficoPro" style="width: 900px; height: 300px;">
        </div>
    </div>
</asp:Content>

