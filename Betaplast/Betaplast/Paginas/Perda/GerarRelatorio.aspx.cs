﻿using BetaPlast;
using BetaPlast.App_Code.Classes;
using BetaPlast.App_Code.Persistencia;
using BetaPlast.Util;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Paginas_Gerente_GerarRelatorio : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {


        ddlTipo.Items.Insert(0, "Selecione");

        if (!Page.IsPostBack)
        {
            ddlSelecionarTipo.DataBind();
            ddlSelecionarTipo.Items.Insert(0, "Selecione");
            ddlSelecionarTipo.Items.Insert(1, "Produto");
            ddlSelecionarTipo.Items.Insert(2, "Matéria-prima");
        }
        Carrega();

    }


    protected void Carrega()
    {


        DataSet produtods = new DataSet();
        PerdaBD bd = new PerdaBD();
        produtods = bd.SelectAllWithProduto();

        grdProduto.DataSource = produtods.Tables[0].DefaultView;
        grdProduto.DataBind();
        grdProduto.HeaderRow.TableSection = TableRowSection.TableHeader;

        DataSet materiads = new DataSet();
        materiads = bd.SelectAllWithMateria();

        grdMateria.DataSource = materiads.Tables[0].DefaultView;
        grdMateria.DataBind();
        grdMateria.HeaderRow.TableSection = TableRowSection.TableHeader;

    }


    protected void btnGerarRelatorio_Click(object sender, EventArgs e)
    {

        Perda perda = new Perda();
        perda.KGPerdidos = Convert.ToDouble(txtPeso.Text);
        perda.QuantidadePerdida = Convert.ToInt32(txtQuantidade.Text);
        perda.Data = Convert.ToDateTime(txtData.Text);
        perda.MotivoPerda = txtMotivo.Text;
        perda.Tipo = ddlSelecionarTipo.SelectedItem.Text;
        if (perda.Tipo != "Selecione")
        {
            if (perda.Tipo == "Produto")
            {
                ProdutoBD produtobd = new ProdutoBD();
                perda.Produto = produtobd.Select(Convert.ToInt32(ddlTipo.SelectedItem.Value));
            }
            else
            {
                MateriaPrimaBD materiaprimabd = new MateriaPrimaBD();
                perda.MateriaPrima = materiaprimabd.Select(Convert.ToInt32(ddlTipo.SelectedItem.Value));
            }
            PerdaBD bd = new PerdaBD();
            if (bd.Insert(perda))
            {
                lblMensagem.Text = "Perda registrada com sucesso!";
                txtPeso.Text = "";
                txtQuantidade.Text = "";
                txtMotivo.Text = "";
                ddlTipo.SelectedItem.Text = "Selecione";
                ddlSelecionarTipo.SelectedItem.Text = "Selecione";
                txtData.Text = "";
                ddlTipo.Focus();
                Carrega();
            }
            else
            {
                lblMensagem.Text = "Erro ao salvar.";
            }
        }
        else
        {
            lblMensagem.Text = "Selecione o tipo de perda";
        }
    }


    protected void ddlSelecionarTipo_SelectedIndexChanged1(object sender, EventArgs e)
    {

        DataSet ds = new DataSet();
        if (ddlSelecionarTipo.SelectedItem.Text == "Produto")
        {
            Label3.Text = "Produto";
            ProdutoBD produtobd = new ProdutoBD();
            ds = produtobd.SelectAll();
            ddlTipo.DataSource = ds.Tables[0].DefaultView;
            ddlTipo.DataTextField = "pro_descricao";
            ddlTipo.DataValueField = "pro_codigo";
            ddlTipo.DataBind();

        }
        else if (ddlSelecionarTipo.SelectedItem.Text == "Matéria-prima")
        {
            Label3.Text = "Matéria-prima";
            MateriaPrimaBD materiaprimabd = new MateriaPrimaBD();
            ds = materiaprimabd.SelectAll();
            ddlTipo.DataSource = ds.Tables[0].DefaultView;
            ddlTipo.DataTextField = "map_nome";
            ddlTipo.DataValueField = "map_codigo";
            ddlTipo.DataBind();
        }
    }
}


