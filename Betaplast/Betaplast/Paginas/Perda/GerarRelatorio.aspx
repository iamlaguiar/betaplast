﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="GerarRelatorio.aspx.cs" MasterPageFile="~/Paginas/Master/Index.master" Inherits="Paginas_Gerente_GerarRelatorio" %>

<asp:Content ID="MenuLateral" ContentPlaceHolderID="MenuLateral" runat="Server">

    <script type="text/javascript">

        $(document).ready(function () {
            $('#<%=grdProduto.ClientID%>').dataTable();
            $('#<%=grdMateria.ClientID%>').dataTable();
        });

        function updateLabel() {
            //ajax
        }


    </script>
</asp:Content>


<asp:Content ID="Conteudo" ContentPlaceHolderID="ConteudoPagina" runat="Server">
    <h3 class="page-header">Lista de perdas</h3>

    <div  id="main" runat="server" style="margin-left: 190px;" class="container">

        <button id="btnModal" type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal" style="margin-left: 32px">Registrar perda</button>
        <div class="container">
            <div class="form-group col-sm-10">

                <!-- modal -->
                <div id="myModal" class="modal fade" role="dialog">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h3 class="modal-title">Registrar perda</h3>
                            </div>
                            <div class="modal-body">
                                <!-- Postback parcial-->
                                <div class="row">
                                    <div class="form-group col-md-4">
                                        <asp:Label ID="lblTitulo" runat="server" Text="Gerar Relatorio De Perdas"></asp:Label>
                                        <asp:Label ID="Label2" runat="server" Text="Selecione o tipo de perda"></asp:Label>
                                        <br />
                                        <asp:DropDownList ID="ddlSelecionarTipo" runat="server" AutoPostBack="true" CssClass="form-control" OnSelectedIndexChanged="ddlSelecionarTipo_SelectedIndexChanged1">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="from-group col-md-4">
                                        <asp:Label ID="Label3" runat="server" Text="Tipo não selecionado"></asp:Label>
                                        <asp:DropDownList ID="ddlTipo" runat="server" CssClass="form-control">
                                        </asp:DropDownList>
                                    </div>
                                    <!-- //Postback parcial -->

                                    <div class="from-group col-md-4">
                                        <asp:Label ID="lblKg" runat="server" Text="Kg:"></asp:Label>
                                        <asp:TextBox ID="txtPeso" runat="server" EnableTheming="True" CssClass="form-control"></asp:TextBox>
                                    </div>

                                    <div class="from-group col-md-4">
                                        <asp:Label ID="Label1" runat="server" Text="Quantidade Perdida:"></asp:Label>
                                        <asp:TextBox ID="txtQuantidade" runat="server" CssClass="form-control"></asp:TextBox>
                                    </div>

                                </div>
                                <br />
                                <div class="row">
                                    <div class="form-group col-md-12">
                                        <asp:Label ID="lblMotivo" runat="server" Text="Motivo Da Perda:"></asp:Label>
                                        <asp:TextBox ID="txtMotivo" runat="server" Height="50px" TextMode="MultiLine" CssClass="form-control"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-4">
                                        <asp:Label ID="Label4" runat="server" Text="Data Da Perda:"></asp:Label>
                                        <asp:TextBox ID="txtData" runat="server" type="date" CssClass="form-control"></asp:TextBox>
                                    </div>
                                    <br />
                                    <div class="form-group col-md-4">
                                        <asp:Button ID="btnGerarRelatorio" runat="server" OnClick="btnGerarRelatorio_Click" Text="Registrar perda" CssClass="btn btn-primary" />
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <asp:Label ID="lblMensagem" runat="server"></asp:Label>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- fim modal -->

                <div class="form col-sm-12">
                    <div id="grid">
                        <asp:Label runat="server" ID="lblGridProduto" Text="Lista de produtos perdidos"></asp:Label>
                        <asp:GridView ID="grdProduto" runat="server" BorderStyle="None" BorderWidth="1px" CellPadding="3" GridLines="Horizontal" CssClass="display">
                            <HeaderStyle HorizontalAlign="Center" BackColor="#0066ff" ForeColor="White" />
                            <AlternatingRowStyle BackColor="#F7F7F7" />
                            <PagerStyle BackColor="#E7E7FF" ForeColor="#4A3C8C" HorizontalAlign="Right" />
                            <RowStyle BackColor="#E7E7FF" ForeColor="#4A3C8C" HorizontalAlign="center" />
                            <SelectedRowStyle BackColor="#738A9C" Font-Bold="True" ForeColor="#F7F7F7" />
                            <SortedAscendingCellStyle BackColor="#F4F4FD" />
                            <SortedAscendingHeaderStyle BackColor="#5A4C9D" />
                            <SortedDescendingCellStyle BackColor="#D8D8F0" />
                            <SortedDescendingHeaderStyle BackColor="#3E3277" />
                        </asp:GridView>
                    </div>

                    <div id="gridmateria">
                        <asp:Label runat="server" ID="Label5" Text="Lista de matérias-prima perdidas"></asp:Label>
                        <asp:GridView ID="grdMateria" runat="server" BorderStyle="None" BorderWidth="1px" CellPadding="3" GridLines="None" CssClass="display">
                            <HeaderStyle BackColor="#0066ff" ForeColor="White" HorizontalAlign="Center" />
                            <AlternatingRowStyle BackColor="#F7F7F7" />
                            <PagerStyle BackColor="#E7E7FF" ForeColor="#4A3C8C" HorizontalAlign="Right" />
                            <RowStyle BackColor="#E7E7FF" ForeColor="#4A3C8C" HorizontalAlign="center" />
                            <SelectedRowStyle BackColor="#738A9C" Font-Bold="True" ForeColor="#F7F7F7" />
                            <SortedAscendingCellStyle BackColor="#F4F4FD" />
                            <SortedAscendingHeaderStyle BackColor="#5A4C9D" />
                            <SortedDescendingCellStyle BackColor="#D8D8F0" />
                            <SortedDescendingHeaderStyle BackColor="#3E3277" />
                        </asp:GridView>
                    </div>
                </div>
            </div>
        </div>
    </div>

</asp:Content>
