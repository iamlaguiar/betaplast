﻿using BetaPlast.App_Code.Persistencia;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Paginas_Perda_Graficos : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            GraficoMateria();
            GraficoProduto();
        }
    }


    private void GraficoProduto()
    {
        StringBuilder strBuilder = new StringBuilder();
        DataSet produtods = new DataSet();
        PerdaBD perdabd = new PerdaBD();

        try
        {
            produtods = perdabd.SelectAllWithProduto();

            strBuilder.Append(@"<script type='text/javascript'>
                                    google.charts.load('current', {packages: ['corechart']});

                                    google.charts.setOnLoadCallback(drawVisualization);

                                    function drawVisualization(){
                                    var data = new google.visualization.DataTable();");
            strBuilder.Append(@"
                                    data.addColumn('string', 'topping');
                                    data.addColumn('number', 'Quantidade');");
            strBuilder.Append(@"data.addRows([");

            for (int i = 0; i < produtods.Tables[0].Rows.Count; i++)
            {
                strBuilder.Append(@"['" + produtods.Tables[0].Rows[i]["Item perdido"] + "', " + produtods.Tables[0].Rows[i]["Quantidade perdida"] + "],");
            }
            strBuilder.Remove(strBuilder.Length - 1, 1);
            strBuilder.Append(@"]);");

            strBuilder.Append(@"
                                    var options = {'title': 'Perdas de produtos',
                                    'width': 700,
                                    'height': 300,
                                     'bar': {groupWidth: '100%', width: 20},
                                     'chartArea': {'width':'100%', left:50, height: '50%'}}; 
                                    var chart = new google.visualization.ColumnChart(document.getElementById('graficoPro'));
                                    chart.draw(data, options);
                                }");
            strBuilder.Append(@"</script>");
            literal.Text = strBuilder.ToString();
        }
        catch (Exception)
        {

            throw;
        }
    }


    private void GraficoMateria()
    {
        StringBuilder strBuilder = new StringBuilder();
        DataSet produtods = new DataSet();
        PerdaBD perdabd = new PerdaBD();

        try
        {
            produtods = perdabd.SelectAllWithMateria();

            strBuilder.Append(@"<script type='text/javascript'>
                                    google.charts.load('current', {packages: ['corechart']});

                                    google.charts.setOnLoadCallback(drawVisualization);

                                    function drawVisualization(){
                                    var data = new google.visualization.DataTable();");
            strBuilder.Append(@"
                                    data.addColumn('string', 'topping');
                                    data.addColumn('number', 'Quantidade');");
            strBuilder.Append(@"data.addRows([");

            for (int i = 0; i < produtods.Tables[0].Rows.Count; i++)
            {
                strBuilder.Append(@"['" + produtods.Tables[0].Rows[i]["Item perdido"] + "', " + produtods.Tables[0].Rows[i]["Quantidade perdida"] + "],");
            }
            strBuilder.Remove(strBuilder.Length - 1, 1);
            strBuilder.Append(@"]);");

            strBuilder.Append(@"
                                    var options = {'title': 'Perdas de matéria prima',
                                    'width': 700,
                                    'height': 300,
                                     'bar': {groupWidth: '100%', width: 20},
                                     'chartArea': {'width':'100%', left:50, height: '50%'}}; 
                                    var chart = new google.visualization.ColumnChart(document.getElementById('graficoMp'));
                                    chart.draw(data, options);
                                }");
            strBuilder.Append(@"</script>");
            literalMp.Text = strBuilder.ToString();
        }
        catch (Exception)
        {

            throw;
        }
    }


}

